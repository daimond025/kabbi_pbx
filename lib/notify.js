//Объект оповещения
//Используется для сохранения переменных и
//маппинга id рест с id канала в Asterisk
function notify(
                order_id,
                status_id,
                domain,
                welcome,
                orderfiles,
                repeatfiles,
                driverfile,
                agentfile,
                wrongkey,
                success,
                cancel,
                driver_phone,
                phone_line,
                callerid,
                call_count_try,
                call_wait_timeout,
                isInfo,
                company_phone,
                order_number)
{
    var self = this;
    self.order_id = order_id;
    self.status_id = status_id;
    self.domain = domain;
    self.welcome = welcome;
    self.orderfiles = orderfiles;
    self.repeatfiles = repeatfiles;
    self.driverfile = driverfile;
    self.agentfile = agentfile;
    self.wrongkey = wrongkey;
    self.success = success;
    self.cancel = cancel;
    self.driver_phone = driver_phone;
    self.phone_line = phone_line;
    self.callerid = callerid;
    self.call_count_try = call_count_try;
    self.call_wait_timeout = call_wait_timeout;
    self.isInfo = isInfo;
    self.state = 'init';
    self.channel = undefined;
    self.company_phone = company_phone;
    self.order_number = order_number;
}

module.exports = notify;