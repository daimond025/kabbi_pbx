//Модуль - абстракция над express
//Реализует rest - сервер для приема асинхронных запросов со стороны app сервера
var util = require('util');
var express = require('express');
var path = require('path');
//var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var util = require('util');
var events = require('events').EventEmitter;
var myConfig = require('../config.js');
var mode = process.env.NODE_ENV;
var config = new myConfig(mode);
var log4js = require('log4js');
log4js.configure(config.logcfg);
var logger = log4js.getLogger('restserver.js');
logger.setLevel(config.loglvl);



function Server(config)
{
    var self = this;
    //self.logger = logger;
    self.config = config;
    self.express = express();
    self.express.use(bodyParser.json());
    self.express.use(bodyParser.urlencoded({extended: false}));
    self.express.use(cookieParser());
    self.express.listen(self.config.port);

    //Методы поддерживаемые сервером - проксируются выше через emit
    //Вся логика в observer.js
    self.express.use('/call/connectagent', function (req, res)
    {
        self.emit('connectagent', req, res);
        logger.debug("api get req method - connect agent:" + req);
    });

    self.express.use('/call/notify', function (req, res)
    {
        self.emit('notify', req, res);
        logger.debug("api get req method - notify:" + req);
    });

    self.express.use('/list', function (req, res)
    {
        self.emit('list', req, res);
        logger.debug("api get req method - list:" + req);
    });

    self.express.use('/trace', function (req, res)
    {
        self.emit('trace', req, res);
        logger.debug("api get req method - list:" + req.body);
    });
    self.express.use('/test', function (req, res)
    {
        self.emit('test', req, res);
        logger.debug("api get req method - test:" + req);
    });

    self.express.use('/updatesipdomains', function (req, res)
    {
        logger.debug("app server req method - updatesipdomains:" + req);
        self.emit('updatesipdomains', req, res);
    });
    self.express.use('/updatesipuser', function (req, res)
    {

        //log.debug("app server req method - updatesipuser:" + req);
        self.emit('updatesipuser', req, res);
    });
    self.express.use('/syncSipUser', function (req, res)
    {

        logger.debug("app server req method - syncSipUser:" + req);
        self.emit('syncSipUser', req, res);
    });
    self.express.use('/syncSipDomain', function (req, res)
    {

        logger.debug("app server req method - syncSipDomain:" + req);
        self.emit('syncSipDomain', req, res);
    });
        self.express.use('/ping', function (req, res)
    {

        logger.debug("app server req method - ping" + req);
        self.emit('ping', req, res);
    });
        self.express.use('/phoneLine', function (req, res)
    {
        logger.debug("app server req method - phoneLine" + req);
        self.emit('phoneLine', req, res);
    });
    self.express.use('/reloaduacreg', function (req, res)
    {

        logger.debug("app server req method - reloaduacreg" + req);
        self.emit('reloaduacreg', req, res);
    });
    self.express.use('/version', function (req, res)
    {

        logger.debug("app server req method - version" + req);
        self.emit('version', req, res);
    });
    self.express.use('/connectdst', function (req, res)
    {

        logger.debug("app server req method - connectdst" + req);
        self.emit('connectdst', req, res);
    });
    self.express.use('/status', function (req, res)
    {
        self.emit('status', req, res);
        logger.debug("api get req method - test:" + req);
    });

}
//Наследуем events
util.inherits(Server, events);

module.exports = Server;
