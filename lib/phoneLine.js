function phoneLine(pbxid, evt) {
    var self = this;
    self.pbxid = pbxid;
    self.username = evt.username;
    self.host = evt.host;
    self.state = evt.state;
}
module.exports = phoneLine;