'use strict';

var Notify = require('./notify.js');
var Tools = require('./tools.js');
var util = require('util');
var myConfig = require('../config.js');
var mode = process.env.NODE_ENV;
var config = new myConfig(mode);
var log = config.log.child({widget_type: 'notifybox.js'});


function Notifybox() {
    var self = this;
    self.box = {};
}

/**
 * 1)Если заказа нет в notifyBox то добавить (вернет true)
 * 2)Если есть уже заказ в notifyBox то,
 *  если  active.state == 'init', то makeNotify в  active,
 *  иначе  makeNotify в buffer
 *  (вернет false)
 * @param {type} obj
 * @param {type} customclid
 * @returns {Boolean}
 */
Notifybox.prototype.push = function (obj, actionid, customclid) {
    var self = this;
    var order = obj.order_id;

    if (self.box[order]) {
        if (self.box[order].active.state == 'init') {
            self.box[order].active = makeNotify(obj, customclid);
            return false;
        } else {
            self.box[order].buffer = makeNotify(obj, customclid);
            return false;
        }
    } else {
        self.box[order] = {};
        self.box[order].active = makeNotify(obj, customclid);
        self.box[order].buffer = null;
        self.box[order].chanactive = undefined;
        self.box[order].actionid = actionid;
        return true;
    }
};

//получает restid канала по номеру заказа
//получает restid канала по номеру заказа
Notifybox.prototype.getrest = function (order) {
    var self = this;
    if (typeof parseInt(order) === "number" &&
        self.box[order] &&
        self.box[order].chanactiv1 &&
        typeof self.box[order].chanactiv1.restid === "string") {
        var id = self.box[order].chanactiv1.restid
        log.info("getrest for order ", order, "return: ", id);
        return id;
    }
    log.info("getrest for order ", order, "return: undefined");
    return undefined;

};
//канал по номеру ордера
Notifybox.prototype.getchan = function (order) {
    var self = this;
    if (typeof order == 'undefined') {
        return undefined;
    }

    if (self.box[order]) {
        return self.box[order].chanactiv1;

    } else {
        return undefined;
    }

};


/*
 * Возвращает box[order].active, иначе undefined
 * TODO почему только activ ? 
 * и зачем этот activ вообще нужен ?
 * из кода поняно, что актив! 
 * пиши норм комментарии.
 * пыщ пыщ ололо.
 */
Notifybox.prototype.get = function (order) {
    var self = this;
    if (typeof order == 'undefined') {
        return undefined;
    }

    if (self.box[order]) {

        if (self.box[order].active) {
            return self.box[order].active;
            log.debug("box return activ");
        } else {
            return undefined;
            log.debug("box return undef");
        }
    } else {
        return undefined;
    }

};


/**
 * 1)Если заказа нет в Notifybox, то вернуть false
 * 2) Если заказ есть в Notifybox и есть буфер, буфер записать в актив, вернуть true
 * 3) Если заказ есть в Notifybox и нет буфера, удалить заказ из Notifybox, вернуть false
 * @param {type} order
 * @returns {Boolean}
 */
Notifybox.prototype.shift = function (order) {
    var self = this;
    if (typeof order == 'undefined') {
        return undefined;
    }
    if (self.box[order]) {
        if (self.box[order].buffer != null) {
            self.box[order].active = self.box[order].buffer;
            self.box[order].buffer = null;
            log.debug("BOX from push", self.box[order])
            return true;
        } else {
            delete self.box[order];
            return false;
        }
    } else {
        return false;
    }
};

Notifybox.prototype.check = function (order) {
    if (typeof order == 'undefined') {
        return undefined;
    }

    var self = this;
    return self.box[order].chanactive;
};

Notifybox.prototype.findChannelByRestId = function (restId, call) {
    log.info("findChannelByRestId", call);
    var self = this;
    for (var i in call) {

    }
    log.info("findChannelByRestId", self.box);

    for (var i in self.box) {


        if (i.chanactive === restId) {
            return i.chanactive;
        }
    }
    return undefined;
}

Notifybox.prototype.drop = function (order) {
    var self = this;
    if (typeof order == 'undefined') {
        return undefined;
    }
    log.debug("delete box order :", self.box[order]);
    if (delete self.box[order]) {
        log.debug("delete box order " + order + " ok");
    } else {
        log.error("delete box order " + order + " fail");
    }

};

Notifybox.prototype.add = function (order) {
    var self = this;


    self.box[order].actionid = new order;
};

//метод возвращает номер restid по идентификатору канала
Notifybox.prototype.getrestid = function (chan, call) {
    var self = this;
    for (var i in call) {
//        if (self.box[i].chanactive){
        if (call[i].channel) {
            log.debug('ACTIVE BOX CHANNELS for restid');
            log.debug(call[i].channel);
            if (call[i].channel == chan) {
                return call[i].restid;
            }
        }
    }

};

//метод возвращает номер restid по идентификатору канала
Notifybox.prototype.getrestid = function (chan, call) {
    var self = this;
    for (var i in call) {
//        if (self.box[i].chanactive){
        if (call[i].channel) {
            //log('ACTIVE BOX CHANNELS for restid');
            log.debug(call[i].channel);
            if (call[i].channel == chan) {
                log.debug(call[i].restid);
                return call[i].restid;
            }
            else {
                return call[i].restid = undefined;
            }

        }
    }

};
//метод возвращает номер заказа по идентификатору actionid
Notifybox.prototype.getorderactid = function (actionid) {
//    self.box[order]
    var self = this;
    for (let i in self.box) {
//        if (self.box[i].chanactive){
        if (self.box[i].actionid === actionid) {
            log.debug("order id: " + self.box[i] + "for actionid" + actionid);
            return self.box[i].active.order_id;


        }
    }

};

//метод возвращает номер заказа по идентификатору channel
Notifybox.prototype.getorderid = function (channel) {
//    self.box[order]
    var self = this;
    for (var i in self.box) {
//        if (self.box[i].chanactive){
        if (typeof evt !== "undefined") {
            if (self.box[i].chanactiv1.channel === channel) {
                log.debug("order id: " + self.box[i] + "for actionid" + channel);
                return self.box[i].active.order_id;
            }
        }
    }

};

//метод возвращает номер заказа по идентификатору restid
Notifybox.prototype.getorderidfromuniqueid = function (restid) {
//    self.box[order]
    var self = this;
    for (var i in self.box) {
        log.debug("box:" + JSON.stringify(self.box[i]));
        if (self.box[i].chanactiv1.restid === restid) {
            log.debug("order id: " + self.box[i] + "for restid" + restid);
            return self.box[i].active.order_id;


        }
    }

};

Notifybox.prototype.setchan = function (order, chan, pbxid, rest) {
    log.info('box set chan:' + chan, 'box set order:', order, 'box set pbxid:' + pbxid);
    var self = this;
    self.box[order].chanactiv1 = {'channel': chan, 'pbxid': pbxid, 'restid': rest};
    log.info([self.box]);
};

Notifybox.prototype.show = function () {
    var self = this;
    return self.box;
};

let makeNotify = function (obj, customclid) {
    var orderfiles = Tools.playList(obj.orderfiles);
    var welcome = Tools.playList(obj.welcome);
    var repeatfiles = Tools.playList(obj.repeatfiles);

    if (customclid) {
        return new Notify
        (
            obj.order_id,
            obj.status_id,
            obj.domain,
            welcome.list,
            orderfiles.list,
            repeatfiles.list,
            obj.driverfile,
            obj.agentfile,
            obj.wrongkey,
            obj.successfile,
            obj.cancel,
            obj.driver_phone,
            obj.phone_line,
            customclid,
            null,
            null,
            true
        );
    } else {
        return new Notify
        (
            obj.order_id,
            obj.status_id,
            obj.domain,
            welcome.list,
            orderfiles.list,
            repeatfiles.list,
            obj.driverfile,
            obj.agentfile,
            obj.wrongkey,
            obj.successfile,
            obj.cancel,
            obj.driver_phone,
            obj.phone_line,
            obj.did,
            obj.call_count_try,
            obj.call_wait_timeout,
            false
        );
    }

};


module.exports = Notifybox;