module.exports.paramsCorrect = function(hash) {
    var result = true;

    for (var key in hash)
    {
        if (!hash[key] || hash[key] == '')
        {
            result = false;
            break;
        }
    }

    return result;
};

module.exports.normalizeParam = function(param) {
    var result = undefined;
    switch (param)
    {
        case '':
            break;
        case null:
            break;
        default:
            result = param;
    }
    return result;
};

module.exports.playList = function(param)
{
    result = {};

    if (param == '')
    {
        result.list = null;
    }
    else
    {
        if ((typeof param) === 'undefined') {
            return result;

        }
        else {
            result.list = param.split(',');
        }
    }

    return result;
};