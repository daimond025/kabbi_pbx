'use strict';

const mysql = require('mysql');
const async = require('async');
const myConfig = require('../config.js');
const mode = process.env.NODE_ENV;
const config = new myConfig(mode);
const log = config.log.child({widget_type: 'dbmanager.js'});
var empty = require('is-empty');


function Dbmanager(sipProxyDB, asteriskDB, arrayDB) {
    var self = this;
    //базаы данных камаилио
    self.sipProxys = [];
    //базы данных астерисков
    self.nodes = [];
    self.arrayDBstatus = [];
    self.log = log;

    sipProxyDB.forEach
    (
        function (sipProxy) {
            self.sipProxys[sipProxy.host] = mysql.createPool
            ({
                connectionLimit: 5,
                host: sipProxy.dbhost,
                user: sipProxy.dbuser,
                password: sipProxy.dbpassword,
                database: sipProxy.database
            });

        }
    );


    asteriskDB.forEach
    (
        function (node) {
            self.nodes[node.id] = mysql.createPool
            ({
                connectionLimit: 5,
                host: node.dbhost,
                user: node.dbuser,
                password: node.dbpassword,
                database: node.database
            });

        }
    );




    arrayDB.forEach
    (
        function (node) {
            self.arrayDBstatus[node.id] = mysql.createPool
            ({
                connectionLimit: 5,
                host: node.dbhost,
                user: node.dbuser,
                password: node.dbpassword,
                database: node.database
            });
            self.arrayDBstatus[node.id].on('connection', function (connection) {
                log.debug(' open connection to database', node.id, "\(", node.dbhost, "\)");
            });

        }
    );
};

Dbmanager.prototype.getDomains = function (cb_func) {
    var self = this;
    var domains = [];
    //TODO disable
    self.sipProxys[config.defaultDB].query('SELECT domain FROM domain',
        function (err, rows) {
            if (err)
                cb_func(err, null)

            rows.forEach
            (
                function (row) {
                    domains.push(row.domain);

                }
            );
            return cb_func(domains);
        });
};


Dbmanager.prototype.getUsers = function (cb_func) {
    var self = this;
    var userList = [];
    log.debug("getUsers ");
    self.sipProxys[config.defaultDB].query('SELECT username,domain,md5_password FROM subscriber',
        function (err, rows) {
            if (err)
                throw err;

            rows.forEach
            (
                function (row) {
                    var currUser = {
                        "name": row.username,
                        "domain": row.domain,
                        "password": row.md5_password
                    }
                    userList[row.username] = currUser;
                }
            );

            return cb_func(userList);
        });

};

Dbmanager.prototype.getUser = function (username, cb_func) {
    var self = this;
    var userList = [];
    var results = [];

    self.sipProxys[config.defaultDB].query({
        sql: 'SELECT username,domain,md5_password FROM `subscriber` WHERE `username` = ?',
        timeout: 4000, // 4s
        values: [username]
    }, function (error, results) {
        if (error) {
            return cb_func(error);
            self.log.error(error);
        }
        if (results) {
            if (results == '') {
                self.log.error(" - getUser Not Found user " + username + " in database kamailio");
                error = "ERROR";
                return cb_func(error);
            }
            self.log.debug(results);
            return cb_func(null, results);
        } else {
            self.log.warn("NF result");
        }
    });
    log.debug(" - getUser update user:" + username);
};

//TODO Переписать с учетом того, что есть выделенные сервера
Dbmanager.prototype.updateNodes = function (domains, cb_func) {
    var self = this;
    log.info("Dbmanager updateNodes", domains);
    if (empty(domains)) {
        log.error("updateNodes input domains is null");
        throw errorEmitted;
    }
    //for (var key in self.nodes) {
    async.forEach(Object.keys(self.nodes), function (key, done) {
        var insert = "insert ignore into `asterisk`.`bit_sip_devices` ( `host`, `nat`, `type`, `allow`, `fromdomain`, `name`, `disallow`, `context`, `qualify`) values ";
        var values = "";
        domains.forEach
        (
            function (domain) {
                values = values + "( '" + domain + "', 'force_rport,comedia', 'peer', 'alaw,ulaw,g729', '" + domain + "', '" + domain + "', 'all', 'test-in', 'yes'),";
            }
        );
        values = values.substring(0, values.length - 1);

        self.nodes[key].query(insert + values, null,
            (err, rows) => {
                if (err) {
                    return cb_func(err);
                }
                self.log.debug("update domains:", rows.affectedRows);
            });
        done();
    }, function (err) {
        if (err) self.log.error(err.message);
        return cb_func('OK');
    })
};

Dbmanager.prototype.updateAster = function (name, pwd, domain, cb_func) {
    var self = this;
    log.info('updateAster for name', name);
    async.forEach(Object.keys(self.nodes), function (key, done) {
            var values = "";
            var insert = "insert ignore into `asterisk`.`bit_sip_devices` ( `host`, `nat`, `type`, `allow`, `fromdomain`, `name`, `disallow`, `context`, `qualify`, `md5secret`, defaultuser,username) values ";
            var context = domain.split('.');
            values = values + "( 'dynamic', 'force_rport,comedia', 'peer', 'alaw,ulaw,g729', '" + domain + "', '" + name + "', 'all', '" + context[0] + "-out', 'yes','" + pwd + "','" + name + "','" + name + "'),";
            values = values.substring(0, values.length - 1);

            self.nodes[key].query
            (
                insert + values,
                function (err, rows) {
                    if (err)
                        return cb_func(err);
                    self.log.info("updateAster", key, "name",name, "affectedRows:",rows.affectedRows);
                }
            )

            done();
        }, function (err) {
            if (err) self.log.error(err.message);
            return cb_func(null, 'OK');
        }
    )
};

Dbmanager.prototype.pwdupdateinAster = function (username, pwd, domain, cb_func) {
    var self = this;
    async.forEach(Object.keys(self.nodes), function (key, done) {
        log.info("update database for host:", key);
        self.nodes[key].query('UPDATE bit_sip_devices SET md5secret = ? WHERE name = ?', [pwd, username]
            , function (err, results) {
                if (err) {
                    log.error(err);
                    return cb_func(err);
                }

                self.log.info('pwdupdateinAster results:', results.message)
                done();
            });

    }, function (err) {
        if (err) self.log.error(err.message);
        return cb_func(null, self.nodes);
    });
};

Dbmanager.prototype.createPhoneLineInKamailio = function (name, pwd, domain, sipServerLocal, sipServerRemote, realm, phoneLine, phoneLineId, tenantId, port, cb_func) {
    var self = this;
    var valueUacreg = "";
    var valueDrgateway = "";
    var valueDrRules = "";
    var insertUacreg = "insert into `kamailio`.`uacreg` ( `id`, `l_uuid`, `l_username`, `l_domain`, `r_username`, `r_domain`, `realm`, `auth_username`, `auth_password`, `auth_proxy`, `expires`) values ";
    var context = domain.split('.');
    valueUacreg = valueUacreg + "('" + phoneLineId + "','" + phoneLine + "', '" + phoneLine + "', '" + domain + "', '" + name + "', '95.213.174.124:5060','" + realm + "','" + name + "','" + pwd + "', 'sip:" + sipServerRemote + ":" + port + "','60'),";
    valueUacreg = valueUacreg.substring(0, valueUacreg.length - 1);

    var insertDrgateway = "insert into `kamailio`.`dr_gateways` ( `type`, `address`, `strip`, `pri_prefix`, `attrs`) values ";
    valueDrgateway = valueDrgateway + "('1','" + sipServerRemote + "', '0', '0', '" + phoneLineId + "')";//type 0 - uac
    valueDrgateway = valueDrgateway.substring(0, valueUacreg.length - 1);

    var insertDrRules = "insert into `kamailio`.`dr_rules` ( `groupid`, `prefix`, `timerec`, `priority`, `routeid`, `gwlist`, `description`) values ";
    valueDrRules = valueDrRules + "('" + phoneLineId + "','','','0','0','" + phoneLineId + "','" + domain + "')";
    valueDrRules = valueDrRules.substring(0, valueUacreg.length - 1);
    if (self.sipProxys[sipServerLocal] === undefined) {

        log.error("NODE_NOT_FOUND");
        var erro = new Error('NODE_NOT_FOUND');
        erro.code = 'NODE_NOT_FOUND';
        return cb_func(erro, null);
    }

    self.sipProxys[sipServerLocal].query
    (
        insertUacreg + valueUacreg,
        function (err, rows) {
            if (err) {
                log.error("Uacreg insert error ");
                return cb_func(err, null);
            }
            if (rows) {
                self.sipProxys[sipServerLocal].query
                (
                    insertDrgateway + valueDrgateway,
                    function (err, rows) {
                        if (err) {
                            log.error("Drgateway insert error for query ", insertDrgateway + valueDrgateway);
                            return cb_func(err, null);
                        }
                        if (rows) {
                            self.sipProxys[sipServerLocal].query
                            (
                                insertDrRules + valueDrRules,
                                function (err, rows) {
                                    if (err) {
                                        log.error("insertDrRules insert error ", insertDrRules + valueDrRules);
                                        return cb_func(err, null);
                                    }
                                    if (rows) {
                                        return cb_func(null, rows.affectedRows);
                                    }
                                }
                            );
                        }
                    }
                );
            }
        }
    );
};

Dbmanager.prototype.updatePhoneLineInKamailio = function (param, sipServerLocal, cb_func) {
    var self = this;

    var id = param.phoneLineId;
    if (typeof(param.phoneLine) == "string") {
        param.r_username = param.phoneLine;
        param.l_username = param.phoneLine;
        param.l_uuid = param.phoneLine;
        delete (param.phoneLine);
    }
    ;
    if (typeof(param.auth_proxy) == "string" && typeof(param.port) == "undefined") {
        var erro = new Error('PORT_REQUERED');
        erro.code = 'PORT_REQUERED';
        return cb_func(erro, null);

    }
    if (typeof(param.auth_proxy) == "undefined" && typeof(param.port) == "string") {
        var erro = new Error('AUTH_PROXY_REQUERED');
        erro.code = 'AUTH_PROXY_REQUERED';
        return cb_func(erro, null);

    }
    if (typeof(param.auth_proxy) == "string" && typeof(param.port) == "string") {
        param.auth_proxy = "sip:" + param.auth_proxy + ":" + param.port;

    }
    delete (param.typeQuery);
    delete (param.typeServer);
    delete (param.typeServerOld);
    delete (param.typeServerOld);
    delete (param.sipServerLocal);
    delete (param.sipServerLocalOld);
    delete (param.phoneLineId);
    delete (param.port);
    delete  (param.tenantId)
    self.log.debug('updatePhoneLineInKamailio with params', param);

    var sql = "update `kamailio`.`uacreg` SET ";
    for (var key in param) {
        if (param.hasOwnProperty(key)) {
            console.log(key + ' ' + param[key]);
            sql = sql + key + " = \"" + param[key] + "\", "
        }
    }
    sql = sql.substring(0, sql.length - 2);
    sql = sql + " where id=" + id;
    log.info('sql:', sql);

    if (self.sipProxys[sipServerLocal] === undefined) {

        log.error("NODE_NOT_FOUND");
        var erro = new Error('NODE_NOT_FOUND');
        erro.code = 'NODE_NOT_FOUND';
        return cb_func(erro, null);
    }

    self.sipProxys[sipServerLocal].query(sql
        , function (error, rows) {
            if (error) {
                log.error(error);
                return cb_func(error, null);
            }
            if (rows) {
                return cb_func(null, rows.affectedRows);
            }
        });
};

Dbmanager.prototype.deletePhoneLineInKamailio = function (phoneLineId, sipServerLocal, cb_func) {
    var self = this;
    let sql = 'DELETE from kamailio.uacreg where id = ' + phoneLineId;
    log.info('sql:', sql);
    if (self.sipProxys[sipServerLocal] === undefined) {

        log.error("NODE_NOT_FOUND");
        var erro = new Error('NODE_NOT_FOUND');
        erro.code = 'NODE_NOT_FOUND';
        return cb_func(erro, null);
    }
    self.sipProxys[sipServerLocal].query(sql
        , function (error, rows) {
            if (error) {
                log.error(error);
                return cb_func(error, null);
            }
            if (rows) {
                return cb_func(null, rows.affectedRows);
            }
        });
};

Dbmanager.prototype.createPhoneLineInAsterisk = function (name, pwd, domain, sipServerLocal, sipServerRemote, realm, phoneLine, phoneLineId, tenantId, port,  cb_func) {
    var self = this;
    var valueUacreg = "";
    var valueDrgateway = "";
    var valueDrRules = "";
    log.info ("name:",name, "pwd:",pwd,"domain:", domain, "sipServerLocal:", sipServerLocal, "sipServerRemote:", sipServerRemote, realm, phoneLine, phoneLineId, tenantId, port);
    var insert = "INSERT INTO `asterisk`.`bit_sip_devices` (`id`, `name`, `context`, `remotesecret`, `transport`, `host`, `dtmfmode`, `fromuser`, `fromdomain`, `insecure`, `qualify`, `allow`, `username`, `defaultuser`, `directmedia`, `callbackextension`, `trunkname`) values ";
    var context = domain.split('.');
    var sql = insert + "('" + phoneLineId+ "','" + phoneLine + "','default-in', '" + pwd + "','udp', '" + sipServerRemote + "', 'rfc2833', '" + name + "','" + domain + "','port,invite','400', 'ulaw;alaw;gsm','" + name + "','" + name + "','no','" + phoneLine + "','" + phoneLine + "')";

    if (self.nodes[sipServerLocal] === undefined) {

        log.error("NODE_NOT_FOUND");
        var erro = new Error('NODE_NOT_FOUND');
        erro.code = 'NODE_NOT_FOUND';
        return cb_func(erro, null);
    }
    log.info(sql);
    self.nodes[sipServerLocal].query(sql
        , function (error, rows) {
            if (error) {
                log.error(error);
                return cb_func(error, null);
            }
            if (rows) {
                return cb_func(null, rows.affectedRows);
            }
        });
};

Dbmanager.prototype.updatePhoneLineInAsterisk = function (param, sipServerLocal, cb_func) {
    var self = this;

    var id = param.phoneLineId;
    if (typeof(param.phoneLine) == "string") {
        param.name = param.phoneLine;
        param.callbackextension = param.phoneLine;
        param.trunkname = param.phoneLine;
        delete (param.phoneLine);
    };
    if (typeof(param.sipServerRemote) == "string") {
        param.host = param.sipServerRemote;
        delete (param.sipServerRemote);
    };
    if (typeof(param.auth_password) == "string") {
        param.remotesecret = param.auth_password;
        delete (param.auth_password);
    }
    if (typeof(param.auth_proxy) == "undefined" && typeof(param.port) == "string") {
        var erro = new Error('AUTH_PROXY_REQUERED');
        erro.code = 'AUTH_PROXY_REQUERED';
        return cb_func(erro, null);

    }
    if (typeof(param.r_username) == "string") {
        param.fromuser = param.r_username;
        param.username = param.r_username;
        param.defaultuser = param.r_username;


        delete (param.r_username);
    };
    if (typeof(param.r_domain) == "string") {
        param.fromdomain = param.r_domain;
        delete (param.r_domain);
    };
    if (typeof(param.realm) == "string") {
        delete (param.realm);
    };
    if (typeof(param.auth_proxy) == "string") {
        param.host = param.auth_proxy;
        delete (param.auth_proxy);
    };

    // if (typeof(param.auth_proxy) == "string" {
    //     param.auth_proxy = "sip:" + param.auth_proxy + ":" + param.port;
    //
    // }
    delete (param.typeQuery);
    delete (param.typeServer);
    delete (param.typeServerOld);
    delete (param.typeServerOld);
    delete (param.sipServerLocal);
    delete (param.sipServerLocalOld);
    delete (param.phoneLineId);
    delete (param.port);
    delete  (param.tenantId)
    delete  (param.auth_proxy)
    self.log.debug('updatePhoneLineInAsterisk with params', param);

    var sql = "update `asterisk`.`bit_sip_devices` SET ";
    for (var key in param) {
        if (param.hasOwnProperty(key)) {
            console.log(key + ' ' + param[key]);
            sql = sql + key + " = \"" + param[key] + "\", "
        }
    }
    sql = sql.substring(0, sql.length - 2);
    sql = sql + " where id=" + id;
    log.info('sql:', sql);

    if (self.nodes[sipServerLocal] === undefined) {

        log.error("NODE_NOT_FOUND");
        var erro = new Error('NODE_NOT_FOUND');
        erro.code = 'NODE_NOT_FOUND';
        return cb_func(erro, null);
    }

    self.nodes[sipServerLocal].query(sql
        , function (error, rows) {
            if (error) {
                log.error(error);
                return cb_func(error, null);
            }
            if (rows) {
                return cb_func(null, rows.affectedRows);
            }
        });
};

Dbmanager.prototype.deletePhoneLineInAsterisk = function (phoneLineId, sipServerLocal, cb_func) {
    var self = this;
    let sql = 'DELETE from asterisk.bit_sip_devices where id = ' + phoneLineId;
    if (self.nodes[sipServerLocal] === undefined) {

        log.error("NODE_NOT_FOUND");
        var erro = new Error('NODE_NOT_FOUND');
        erro.code = 'NODE_NOT_FOUND';
        return cb_func(erro, null);
    }
    log.info(sql);
    self.nodes[sipServerLocal].query(sql
        , function (error, rows) {
            if (error) {
                log.error(error);
                return cb_func(error, null);
            }
            if (rows) {
                return cb_func(null, rows.affectedRows);
            }
        });
};
/**
 * Check insert to db
 * @param key
 * @param value
 * @return {Promise}
 */

Dbmanager.prototype.checkInsert = function (key, value, id) {
    var self = this;
    return new Promise((resolve, reject) => {
        let insertVals = [key, value];
        self.arrayDBstatus[id].query("INSERT INTO `tbl_check` set `key` = ?, `value` = ?", insertVals,
            (err, result) => {
                if (err) {
                    self.log.error("Dbmanager checkInsert in ", id, ",error:", err.message, "(", err.sql, ")");
                    return reject(err);
                } else {
                    resolve();
                }
            });
    });
};

/**
 * Check select from db
 * @param key
 * @return {Promise}
 */

Dbmanager.prototype.checkSelect = function (key, id) {
    const self = this;
    return new Promise((resolve, reject) => {
        const selectVals = [key];
        self.arrayDBstatus[id].query("SELECT * from `tbl_check` where `key` = ?", selectVals,
            (err, result) => {
                if (err) {
                    self.log.error("Dbmanager checkInsert in ", id, ",error:", err.message, "(", err.sql, ")");
                    return reject(err);
                } else {
                    resolve();
                }
            })
    });
};


/**
 * Check update to db
 * @param key
 * @param value
 * @return {Promise}
 */

Dbmanager.prototype.checkUpdate = function (key, value, id) {
    const self = this;
    return new Promise((resolve, reject) => {
        const updateVals = [value, key];
        self.arrayDBstatus[id].query('UPDATE  `tbl_check` SET `value` = ? where `key` = ?', updateVals,
            (err, result) => {
                if (err) {
                    self.log.error("checkUpdate in ", id, ",error:", err.message, "(", err.sql, ")");
                    return reject(err);
                } else {
                    resolve();
                }
            })
    });
};


/**
 * Check delete from db
 * @param key
 * @return {Promise}
 */
Dbmanager.prototype.checkDelete = function (key, id) {
    const self = this;
    return new Promise((resolve, reject) => {
        const updateVals = [key];
        self.arrayDBstatus[id].query('DELETE from `tbl_check` where `key` = ?', updateVals,
            (err, result) => {
                if (err) {
                    self.log.error("checkDelete in ", id, ",error:", err.message, "(", err.sql, ")");
                    return reject(err);
                } else {
                    resolve();
                }
            });
    });
};

Dbmanager.prototype.list = function () {
    const self = this;
    return self.arrayDBstatus;
}
module.exports = Dbmanager;