'use strict';
const myConfig = require('../config');
const mode = process.env.NODE_ENV;
const config = new myConfig(mode);
const packageInfo = require('../package.json');
var log = config.log.child({widget_type: 'ami.js'})
const uuid = require('uuid/v4');
const async = require('async');


function version(res) {
    const name = packageInfo.name;
    const version = packageInfo.version;
    res.setHeader('Content-Type', 'text/plain');
    let info = `${name} v${version}
work on nodejs ${process.version}`;
    res.send(info);
}


function status(manager, dbmanager, res) {
    let asteriskStatus, dbStatus = '';
    let isNot = ' ';
    let hasError = false;
    manager.getStatusManagers()
        .then((asteriskStatusArr) => {
            asteriskStatus = asteriskStatusArr.join('');
        })
        .catch(asteriskStatusArr => {
            asteriskStatus = asteriskStatusArr.join('');
            hasError = true;
        })
        .then(() => {
            return dbChecker(dbmanager);
        })
        .then((dbStatusArr) => {

            dbStatus = dbStatusArr.join('');
        })
        .catch((dbStatusArr) => {
            dbStatus = dbStatusArr.join('');
            hasError = true;
        })
        .then(() => {
            if (hasError) {
                isNot = ' NOT ';
            }
            res.setHeader('Content-Type', 'text/plain');
            res.send(`${packageInfo.name} v${packageInfo.version}
work on nodejs ${process.version}

${dbStatus}${asteriskStatus}
SERVICE${isNot}OPERATIONAL`);
        });
}


/**
 * Check db
 * @return {Promise}
 */
function dbChecker(dbmanager) {
    return new Promise((resolve, reject) => {
        let key = uuid();
        let listDb = dbmanager.list();
        let active = [];
        async.forEach(Object.keys(listDb), function (id, done) {
            dbmanager.checkInsert(key, 'push_test_insert', id)
                .then(() => {
                    return dbmanager.checkSelect(key,id)
                })
                .then(() => {
                    return dbmanager.checkUpdate(key, 'push_test_update',id)
                })
                .then(() => {
                    return dbmanager.checkDelete(key,id)
                })
                .then(() => {
                    active.push("[ OK    ] Database connection, CRUD " + "\"" + id + "\"\n");
                    done();
                })
                .catch(err => {
                    active.push("[ FAIL  ] Database connection, CRUD " + "\"" + id + "\" ("+ err.message + ")\n");
                    done(err);
                });
        }, function (err) {
            if (err) {
                reject(active);
            }
            resolve(active);
        })
    })
}

module.exports = {
    version: version,
    status: status
};
