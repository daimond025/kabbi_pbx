//Абстракция над node-rest-client служит для rest запросов к api app сервера
var Client = require('node-rest-client').Client;
var client = new Client();
var util = require('util');
var myConfig = require('../config.js');
var mode = process.env.NODE_ENV;
var config = new myConfig(mode);
var log = config.log.child({widget_type: 'restclient.js'});


function Restclient(config, logger)
{
    var self = this;
    self.logger = logger;
    self.childLogger = null;
    self.host = config.host;
    self.port = config.port;
    self.protocol = config.protocol;
    self.client = new Client();
    self.args =
            {
                requestConfig: {
                    timeout: 20000,
                    noDelay: true,
                    keepAlive: true,
                    keepAliveDelay: 1000
                },
                responseConfig: {
                    timeout: 5000
                }
            };
}
;

//Метод оповещает app сервер о новом входящем вызове
//ответ проксируется в observer.js через cb_func
//в ответе должено содержаться первыое дейстивие сценария вызова Както стремно что логи через параметры ходят?!
//наверное да, но нужно привязать их к событию
Restclient.prototype.restIncoming = function (restid, did, clid, frominfo, childLogger, cb_func)
{
    var self = this;
    //self.log = childLogger.child({widget_type: 'restIncoming'});
    self.log = log;


    var req = self.client.get(self.protocol + self.host + ":"
            + self.port + "/"
            + "call/incoming?"
            + "did=" + did
            + "&clid=" + clid
            + "&frominfo=" + frominfo
            + "&callid=" + restid,
            self.args,
            function (data, response, prmlog, log)
            {


                    if (response.statusCode == 200)
                    {
                        self.log.info(restid,{req: response.socket._httpMessage.path}, {res: data.toString('utf8')},{req_id: restid});
                        //self.log.info('restIncoming recieved response');
                        //log.info({res: data}, 'stop');
                        cb_func(data);
                    } else
                    {
                        self.log.error(restid,{res: data.toString('utf8')});
                        cb_func('{"action":"Err"}');
                    }


            })
    //log.info('getting from app server');
    log.info(restid, 'Send restIncoming request',{req: req.path}, {req_id: restid} );


    req.on('requestTimeout', function (req)
    {
        self.log.error(restid,{req: req.options}, {req_id: restid}, 'requestTimeout');
        req.abort();
    });

    req.on('responseTimeout', function (res)
    {
        self.log.error(restid," response has expired");

    });

    req.on('error', function (err)
    {
        self.log.error(restid,' request error', err);
        self.log.error(req);
    });
};

//Метод оповещает app сервер о завершении предыдущего действия
//ответ проксируется в observer.js через cb_func
//в ответе должено содержаться следующее дейстивие сценария вызова
Restclient.prototype.restComplete = function (restid, state, childLogger, cb_func)
{
    var self = this;
    self.log = log;
    //childLogger.child({req_id: restid}, true);
    //self.log = childLogger.child({widget_type: 'restComplete'});
    var req = self.client.get(self.protocol + self.host + ":"
            + self.port + "/"
            + "call/actcomplete?"
            + "callid=" + restid
            + "&state=" + state,
            self.args,
            function (data, response)
            {
                    if (response.statusCode == 200)
                    {
                        self.log.info(restid,'restComplete' ,{req:response.socket._httpMessage.path}, {res: data.toString('utf8')},{req_id: restid});
                       // self.log.info('recieved response');
                        cb_func(data);
                    } else
                    {
                        self.log.error(restid,{res: data.toString('utf8')});
                        cb_func('{"action":"Err"}');
                    }

            });
    //log = childLogger.child({req_id: restid}, true);
    
   // self.log.info('getting from app server');
    self.log.info(restid, 'send restComplete request',{req: req.path}, {req_id: restid});
    req.on('requestTimeout', function (req)
    {
        self.log.error("request has expired");
        req.abort();
    });

    req.on('responseTimeout', function (res)
    {
        self.log.info("response has expired");
    });

    req.on('error', function (err)
    {
        self.log.error('request error', err);
        self.log.error(req);
    });
};

//Метод оповещает app сервер о разрыве sip сессии
//ответ проксируется в observer.js через cb_func
//в ответе должено содержаться сообщение об успешном удалении вызова из памяти app сервера
Restclient.prototype.restHangup = function (restid, leg, state, childLogger, cb_func)
{
    var self = this;
    self.log = log
    log = childLogger.child({req_id: restid}, true);
    log = childLogger.child({widget_type: 'restHangup resclient.js'});
    var req = self.client.get(self.protocol + self.host + ":"
            + self.port + "/"
            + "call/hangup?"
            + "callid=" + restid
            + "&leg=" + leg
            + "&state=" + state,
            self.args,
            function (data, response)
            {

                    if (response.statusCode == 200)
                    {
                        self.log.info(restid, 'restHangup answer',{req:response.socket._httpMessage.path},{res: data.toString('utf8')}, {req_id: restid});
                        log.debug(restid,{res: data.toString('utf8')}, ' restHangup recieved data');
                        cb_func(data);
                    } else
                    {
                        self.log.error(restid,{res: data.toString('utf8')});
                        cb_func('{"action":"Err"}');
                    }

            });
    //log = childLogger.child({req_id: restid}, true);
    //log = childLogger.child({widget_type: 'get_app_server'});
    //self.log.info('getting from app server');
    //self.log.info({req: req.options}, {req_id: restid}, 'send request')
    log.info(restid, 'Send to app server restHangup request',{req_id: restid});
    req.on('requestTimeout', function (req)
    {
        self.log.error(restid,"restHangup request has expired");
        req.abort();
    });

    req.on('responseTimeout', function (res)
    {
        self.log.error(restid,"restHangup response has expired");
    });

    req.on('error', function (err)
    {
        self.log.info('request error', err);
        self.log.info(req);
    });
};

Restclient.prototype.restConnectAgentFail = function (restid, agent, state, childLogger, cb_func)
{
    var self = this;
    var logger = logger;
    self.log = log
    log = childLogger.child({req_id: restid}, true);
    log = childLogger.child({widget_type: 'restConnectFail resclient.js'});
    var req = self.client.get(self.protocol + self.host + ":"
            + self.port + "/"
            + "call/connectagentfail?"
            + "callid=" + restid
            + "&agent=" + agent
            + "&state=" + state,
            self.args,
            function (data, response)
            {
                    if (response.statusCode == 200)
                    {
                        log.info(restid, {req:response.socket._httpMessage.path},{res: data.toString('utf8')},{req_id: restid});
                        cb_func(data);
                    } else
                    {
                        log.error(restid,{res: data.toString('utf8')});
                        cb_func('{"action":"Err"}');
                    }

            });

    
    //log = childLogger.child({widget_type: 'get_app_server'});
    //log.info('getting from app server');
    log.info({req: req.options}, {req_id: restid}, 'send request');
    req.on('requestTimeout', function (req)
    {
        log.error("request has expired");
        req.abort();
    });

    req.on('responseTimeout', function (res)
    {
        log.error("response has expired");
    });

    req.on('error', function (err)
    {
        log.error('request error', err);
        log.error(req);
    });
};

Restclient.prototype.restNotifyfinished = function (order_id, status_id, domain, result, restid, childLogger)
{
    var self = this;
    self.log = log
    log = childLogger.child({req_id: order_id}, true);
    log = childLogger.child({widget_type: 'restsendNotify resclient.js'});
    var req = self.client.get(self.protocol + self.host + ":"
            + self.port + "/"
            + "call/notifyfinished?"
            + "order_id=" + order_id
            + "&status_id=" + status_id
            + "&domain=" + domain
            + "&result=" + result,
            self.args,
            function (data, response)
            {
                    if (response.statusCode == 200)
                    {
                        log.info(restid, {req:response.socket._httpMessage.path},{res: data.toString('utf8')},{req_id: restid});
                        self.log.info({req_id: order_id},'restNotifyfinished' ,{res: data.toString('utf8')}, response.socket._httpMessage.path);
                        //cb_func(data);
                    } else
                    {
                        log.error(restid,{res: data.toString('utf8')});
                        cb_func('{"action":"Err"}');
                    }

            });
    log.info(restid,'getting from app server', 'send  restNotifyfinished request',{req: req.options},{req_id: restid});
    req.on('requestTimeout', function (req)
    {
        self.log.error("request has requestTimeout");
        req.abort();
    });

    req.on('responseTimeout', function (res)
    {
         log.error("response has expired");
    });

    req.on('error', function (err)
    {
        log.error('request error', err);
        log.error(req);
    });
};

//TODO Возможно нужно возвращать в коллбеке результат ?
Restclient.prototype.restsendNotify = function (evt, restid, childLogger) {
    var self = this;
    var args = {
        data: evt,
        headers: {
            "Content-Type": "application/json"
        }
    };
    log = childLogger.child({req_id: restid}, true);
    log = childLogger.child({widget_type: 'restsendNotify resclient.js'});
    var req = self.client.post(self.protocol + self.host + ":"
            + self.port + "/" + "call/asterisk_event",
            args,
            function (data, response)
            {

                    if (response.statusCode == 200)
                    {
                        log.info(restid, {req:response.socket._httpMessage.path},{res: data.toString('utf8')},{req_id: restid});
                    } else
                    {
                        log.error(restid,{res: data.toString('utf8').body});
                    }

            }
    )
    //log = childLogger.child({req_id: restid}, true);
    log.info(restid, 'send CDR to phone-api','source: ', evt.source,'destination: ', evt.destination,'disposition: ', evt.disposition, 'uniqueid:', evt.uniqueid, {req_id: restid} );
};

Restclient.prototype.restSendConnectAgentStatus = function (agent,state,type,cb_func) {
    var self = this;
    var args = {
        // data:  "agent: " + agent +"\nstate:" +state + "\ntype:" + type + "\n",
        data: {
            agent: "" + agent,
            state: "" + state,
            type: "" + type
        },
        headers: {
            "Content-Type": "application/x-www-form-urlencoded"
        }

    };
    log.info({ agent: agent,
        state: state,
        type: type});

    //log = childLogger.child({widget_type: 'restSendConnectAgentStatus resclient.js'});
    var req = self.client.post(self.protocol + self.host + ":"
        + self.port + "/v1/" + "operator/set-status",
        args,
        function (data, response)
        {

            if (response.statusCode == 200)
            {
                log.info({req:response.socket._httpMessage.path},{res: data.toString('utf8')});
                return cb_func(null,data);


            } else
            {
                log.error({res: data.toString('utf8')});
            }

        }
    )
    log.debug(req)
 };

module.exports = Restclient;
