/**
 * Created by user on 05.07.2017.
 */
var myConfig = require('../config.js');
var mode = process.env.NODE_ENV;
var config = new myConfig(mode);
var log = config.log.child({widget_type: 'rpc.js'});

var Client = require('node-rest-client').Client;
var client = new Client();


function Rpc(config)
{
    var self = this;
    self.config = config;
    //self.childLogger = null;
    self.host = config.host;
    self.port = config.port;
    self.protocol = config.protocol;
    self.client = new Client();
    self.log = log;
//TODO эти таймауты не работают. разобраться почему.
    self.args =
    {
        requestConfig: {
            timeout: 5000
        },
        responseConfig: {
            timeout: 5000
        }
    };


};

/**
 * Шлет в камаилио команды через RPCXML.
 * в камаилио использует порт для SIP - 5060,
 * модуль xmlrpc
 *
 * @param {type} string
 * @param {type} string
 * @returns {Boolean}
 */

Rpc.prototype.sendCommand = function (node,method ,cb_func) {
    var self = this;
    self.log = log;
    self.log.info("sendCommand", node,method);
    var body = '<?xml version="1.0" encoding="utf-8"?>' +
        '<methodCall>'+
        '<methodName>' +
            method +
        '</methodName>' +
        '<params>' +
        '</params>' +
        '</methodCall>';
    var args = {
        data: body,
        headers: {
            "Content-Type": "text/xml"
        }
    };

    //Ищем в конфиге ноду
    self.config.forEach
    (
        function (row) {
            if (row.host == node){
                //шлем постом XMLRPC запрос  
                var req = self.client.post(row.protocol + row.host + ":"
                    + row.port + "/" + "RPC",
                    args,
                    function (data, response)
                    {
                        if (cb_func && typeof cb_func == 'function') {
                            if (response.statusCode == 200)
                            {
                                //  log.info({req_id: restid}, {res: data}, response.socket._httpMessage.path);
                                log.info('RPCXML recieved response');
                                cb_func(null,data.toString());
                            } else
                            {
                                log.error({res: response});
                            }
                        }
                    }
                )

                req.on('requestTimeout', function (req)
                {
                    self.log.error("request has expired");
                    req.abort();
                });

                req.on('responseTimeout', function (res)
                {
                    self.log.error("response has expired");
                });

                req.on('error', function (err)
                {
                    self.log.info('request error', err);
                    self.log.error(req);
                    cb_func(err);

                });
            }
        }
    )



};

module.exports = Rpc;