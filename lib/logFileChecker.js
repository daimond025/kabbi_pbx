'use strict';
const fs = require('fs');
function checkLogFile() {
    try {
        //проверка на существовании директории
        fs.statSync('log/');
        console.log('Log folder exist');
        try {
            fs.statSync('log/bunyan.log');
            console.log('Log file exist');
        }
            //если деректория есть, а файла лога нет, создадим его
        catch (err) {
            console.log('Create file bunyan.log');
            fs.writeFile('./log/bunyan.log', '');
        }
    }
    catch(err) {
        //если директория не существует, создаем её и файл лога
        console.log('Create folder log');
        fs.mkdirSync("log/", '777');
        console.log('Create file bunyan.log');
        fs.writeFile('./log/bunyan.log', '');
    }

}

module.exports =  checkLogFile();
