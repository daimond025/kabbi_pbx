//Модуль обеспечивает интерфейс с Asterisk по AMI
//Абстракция поверх asterisk-manger
'use strict'
var myConfig = require('../config.js');
var mode = process.env.NODE_ENV;
var config = new myConfig(mode);
var connector = require('asterisk-manager');
var util = require('util');
var events = require('events').EventEmitter;
var crypto = require('crypto');
var log = config.log.child({widget_type: 'ami.js'});
const uuid = require('uuid/v4');

var generate_key = function () {
    var sha = crypto.createHash('sha1');
    sha.update(Math.random().toString());
    return sha.digest('hex');
};


function Manager(config, logger) {
    var self = this;
    //Массив коннекторов к Asterisk
    self.amis = {};
    self.logger = logger;
    self.childLogger = null;

    config.forEach(function (pbx) {
        //Инициализируем новый коннектор - параметры в config.js
        self.amis[pbx.id] = new connector(pbx.port,
            pbx.addr,
            pbx.user,
            pbx.secret,
            true);

        self.amis[pbx.id].id = pbx.id;
        self.amis[pbx.id].tenant = pbx.tenant;
        self.amis[pbx.id].keepConnected();
        self.amis[pbx.id].on('managerevent', function (evt) {
            self.emit('pbx', pbx.id, evt);
        });
    });
}
;
//Насследуем events
util.inherits(Manager, events);

Manager.prototype.getStatusManagers = function () {
    return new Promise((resolve, reject) => {
        var self = this;
        let active = [];
        let asteriskName = [];
        let asteriskStatus = [];
        Object.keys(self.amis).forEach(function (managerid) {
            if (self.amis[managerid].isConnected()) {
                active.push("[ OK    ] Asterisk AMI connection " + "\"" + managerid + "\"\n");
                active.push(true);
            } else {
                active.push("[ FAIL  ] Asterisk AMI connection " + "\"" + managerid + "\"\n");
                active.push(false);
            }
        });
        if (active.length === 0) {
            log.error("getActiveManagers not found active media servers:", active);
        }

        for (let i in active) {
            if (typeof (active[i]) !== "boolean") {
                asteriskName.push(active[i]);
            } else {
                asteriskStatus.push(active[i]);
            }
        }

        if (asteriskStatus.every(isPositive)) {
            resolve(asteriskName);
        } else {
            reject(asteriskName);

        }
    });

};

Manager.prototype.getActiveManagers = function () {
    var self = this;
    var active = [];
    Object.keys(self.amis).forEach(function (managerid) {
        if (self.amis[managerid].isConnected() && self.amis[managerid].tenant.length == 0)
            active.push(managerid);
    });
    if (active.length == 0) {
        log.warn("getActiveManagers not found active media servers:", active);
    } else {
        log.warn("getActiveManagers return", active);
    }
    return active;
};

Manager.prototype.getActiveManagersForTenant = function (tenant) {
    var self = this;
    var active = [];
    //проверка на выделенный медиа сервер для арендатора
    Object.keys(self.amis).forEach(function (managerid) {
        if (self.amis[managerid].isConnected()) {
            self.amis[managerid].tenant.forEach(function (tenantarr) {
                if (tenant == tenantarr) {
                    //active.push("asterisk:  " + managerid + " tenant: " + tenantarr);
                    active.push(managerid)
                }
            });
        }
        log.info(managerid);
    });
    //если выделенных серверов нет
    if (active.length == 0) {
        log.info("dedicated media server not found, we send our servers")
        //отдадим наш медиасервер
        return self.getActiveManagers();
    } else {
        log.info("dedicated media server found: ", active);
        return active;
    }

};

//Воспроизводит КПВ в канале call.channel
Manager.prototype.doRinging = function (call, timeout) {
    var self = this;
    var logger = self.logger;
    self.amis[call.pbxid].action
    ({
        'action': 'setvar',
        'actionid': generate_key(),
        'channel': call.channel,
        'variable': 'TIMEOUT',
        'value': timeout
    }, function (err, res) {
        if (err) {
            //log.error(call.restid,util.inspect(err));
            log.warn(call.restid, 'Ami pbx for action doRinging', {'response': err.response}, {'message': err.message});
            //log.error(call);
        } else {
            //Перенаправляется в контекст в котором запускается MOH и ренерируется событие
            self.amis[call.pbxid].action
            ({
                    'action': 'redirect',
                    'actionid': generate_key(),
                    'channel': call.channel,
                    'context': 'RINGING',
                    'exten': call.did,
                    'priority': 1
                }, function (err, res) {
                    if (err) {
                        //log.error(call.restid,util.inspect(err));
                        log.warn(call.restid, 'Ami pbx for action doRinging', {'response': err.response}, {'message': err.message});

                    } else {
                        log.info(call.restid, 'Ami pbx for action doRinging', {'response': res.response}, {'message': res.message});
                    }
                }
            );
        }
    });
};


//Поднимает трубку в канале call.channel
Manager.prototype.doAnswer = function (did, call, method, domain) {
    var self = this;
    //установит в переменные астера домен, коотрый получили от phone api
    //домен определил по номеру телефону
    self.amis[call.pbxid].action
    ({
        'action': 'setvar',
        'actionid': generate_key(),
        'channel': call.channel,
        'variable': 'DOMAINS',
        'value': domain

    }, function (err, res) {
        if (err) {
            //log.error(call.restid,util.inspect(err));
            log.warn(call.restid, 'Ami pbx for action doAnswer', {'response': err.response}, {'message': err.message});

        } else {
            log.info(call.restid, 'Ami pbx for action doAnswer', {'response': res.response}, {'message': res.message});


            self.amis[call.pbxid].action
            ({
                    'action': 'redirect',
                    'actionid': generate_key(),
                    'channel': call.channel,
                    'context': 'ANSWER',
                    'exten': did,
                    'priority': 1
                }, function (err, res) {
                    if (err) {
                        //log.error(call.restid,util.inspect(err));
                        log.warn(call.restid, 'Ami pbx for action doAnswer', {'response': err.response}, {'message': err.message});

                    } else {
                        log.info(call.restid, 'Ami pbx for action doAnswer', {'response': res.response}, {'message': res.message});
                    }
                }
            );
        }
    });
};


//Поднимает трубку в канале call.channel
Manager.prototype.doProgress = function (did, call, method, domain) {
    var self = this;
    //установит в переменные астера домен, коотрый получили от phone api
    //домен определил по номеру телефону
    self.amis[call.pbxid].action
    ({
        'action': 'setvar',
        'actionid': generate_key(),
        'channel': call.channel,
        'variable': 'DOMAINS',
        'value': domain

    });

    self.amis[call.pbxid].action
    ({
        'action': 'redirect',
        'actionid': generate_key(),
        'channel': call.channel,
        'context': 'PROGRESS',
        'exten': did,
        'priority': 1

    });
};
//Разрывает вызов в канале call.channel
Manager.prototype.doHangup = function (call, cause, cb_func) {
    var self = this;
    var logger = self.logger;
    self.amis[call.pbxid].action
    ({
        'action': 'hangup',
        'actionid': generate_key(),
        'channel': call.channel,
        'cause': cause
    }, function (err, res) {
        if (err) {
            log.warn(call.restid, 'Ami pbx for action doHangup', {'response': err.response}, {'message': err.message});
        } else {
            log.info(call.restid, 'Ami pbx for action doHangup', {'response': res.response}, {'message': res.message});
            return cb_func;
        }
    });
};

//Разрывает вызов в канале call.channel
Manager.prototype.doHangupChannel = function (channel, pbxid, cause, cb_func) {
    var self = this;
    self.amis[pbxid].action
    ({
        'action': 'hangup',
        'actionid': generate_key(),
        'channel': channel,
        'cause': cause
    }, function (err, res) {
        if (err) {
            log.warn(channel, 'Ami pbx for action doHangupChannel', {'response': err.response}, {'message': err.message});
        } else {
            log.info(channel, 'Ami pbx for action doHangupChannel', {'response': res.response}, {'message': res.message});
            return cb_func;
        }
    });
};

//Запускает музыку на ожидании в канале call.channel
//Класс передаваемый в mclass должен быть объявлен в Asterisk
Manager.prototype.doMoh = function (call, mclass, timeout) {
    var self = this;
    var logger = self.logger;
    //Устанавливается переменная канала CAHENNELMOH
    self.amis[call.pbxid].action
    ({
        'action': 'setvar',
        'actionid': generate_key(),
        'channel': call.channel,
        'variable': 'CHANNELMOH',
        'value': mclass
    }, function (err, res) {
        if (err) {
            log.warn(call.restid, 'Ami pbx for action doMoh', {'response': err.response}, {'message': err.message});
        } else {
            self.amis[call.pbxid].action
            ({
                'action': 'setvar',
                'actionid': generate_key(),
                'channel': call.channel,
                'variable': 'TIMEOUT',
                'value': timeout
            }, function (err, res) {
                if (err) {
                    log.warn(call.restid, 'Ami pbx for action doMoh', {'response': err.response}, {'message': err.message});
                } else {
                    //Перенаправляется в контекст в котором запускается MOH и ренерируется событие
                    self.amis[call.pbxid].action
                    ({
                        'action': 'redirect',
                        'actionid': generate_key(),
                        'channel': call.channel,
                        'context': 'MUSICONHOLD',
                        'exten': call.did,
                        'priority': 1
                    });
                    log.info(call.restid, 'Ami pbx for action doMoh', {'response': res.response}, {'message': res.message});
                }
            });
        }
    });
};

//Перенаправляет вызов в контекст соединение с оператором
Manager.prototype.connectAgent = function (call, agent, childLogger, cb_func) {
    var self = this;
    var logger = self.logger;
    self.log = childLogger.child({widget_type: 'ami.js connectAgent'});
    if (typeof call === undefined) {
        self.log.info({req_id: call.restid}, 'connectAgent  to ', agent, 'did: ', call.did);
    }
    else {
        self.log.info('connectAgent  to ', agent, 'call: ', call);
    }
    //Выставляет переменную канала AGENT - формат - AOR оператора
    var actionid = generate_key();
    self.amis[call.pbxid].action
    ({
        'action': 'setvar',
        'actionid': actionid,
        'channel': call.channel,
        'variable': 'AGENT',
        'value': agent
    }, function (err, res) {
        if (err) {
            log.warn(call.restid, 'Ami pbx for action connectAgent', {'response': err.response}, {'message': err.message});
            return cb_func(err, null);
        } else {
            //Редирект в канал для выполнения Dial и перехвата исключений
            self.amis[call.pbxid].action
            ({
                'action': 'redirect',
                'actionid': generate_key(),
                'channel': call.channel,
                'context': 'CONNECTAGENT',
                'exten': call.did,
                'priority': 1
            });
            log.info(call.restid, 'Ami pbx for action connectAgent', {'response': res.response}, {'message': res.message});
            return cb_func(null, null);
        }
    });
};

//Перенаправляет вызов в контекс проигрывающий файл и завершающий вызов
Manager.prototype.doPlayHangup = function (call, file) {
    var self = this;
    var logger = self.logger;
    //Выставляет переменную канала AGENT - формат - AOR оператора
    self.amis[call.pbxid].action
    ({
        'action': 'setvar',
        'actionid': generate_key(),
        'channel': call.channel,
        'variable': 'NOWPLAY',
        'value': file
    }, function (err, res) {
        if (err) {
            log.warn(call.restid, 'Ami pbx for action doPlayHangup', {'response': err.response}, {'message': err.message});
        } else {
            //Редирект в канал для выполнения Dial и перехвата исключений
            self.amis[call.pbxid].action
            ({
                'action': 'redirect',
                'actionid': generate_key(),
                'channel': call.channel,
                'context': 'PLAYHANGUP',
                'exten': call.did,
                'priority': 1
            });
            log.info(call.restid, 'Ami pbx for action doPlayHangup', {'response': res.response}, {'message': res.message});
        }
    });
};

//Перенаправляет вызов в контекст входящих вызовов на службу такси для соединения
//с оператором из NOTIFY
Manager.prototype.dialService = function (call, did, clid, say) {
    var self = this;
    var logger = self.logger;
    //Выставляет переменную канала NOTIFY признак что вызов из оповещения
    self.amis[call.pbxid].action
    ({
        'action': 'setvar',
        'actionid': generate_key(),
        'channel': call.channel,
        'variable': 'NOTIFY',
        'value': '1'
    }, function (err, res) {
        if (err) {
            log.error(util.inspect(err));
            log.error(call);
        } else {
            self.amis[call.pbxid].action
            ({
                'action': 'setvar',
                'actionid': generate_key(),
                'channel': call.channel,
                'variable': 'SAY',
                'value': say
            }, function (err, res) {
                if (err) {
                    log.error(util.inspect(err));
                    log.error(call);
                } else {
                    self.amis[call.pbxid].action
                    ({
                        'action': 'setvar',
                        'actionid': generate_key(),
                        'channel': call.channel,
                        'variable': 'SETCLID',
                        'value': clid
                    }, function (err, res) {
                        if (err) {
                            log.error(util.inspect(err));
                            log.error(call);
                        } else {
                            self.amis[call.pbxid].action
                            ({
                                'action': 'redirect',
                                'actionid': generate_key(),
                                'channel': call.channel,
                                'context': 'CONNECTSERVICE',
                                'exten': did,
                                'priority': 1
                            }, function (err, res) {
                                logger.info("CONNECTSERVICE DID: " + did);
                                if (err) {
                                    log.error(util.inspect(err));
                                    log.error(call);
                                }
                            });
                        }
                    });
                }
            });
        }
    });
};

//Для соединения с любым номером(например с водителем)
Manager.prototype.doDial = function (call, did, domain, say, phline, typemusic, classmusic, timeout, cb_func) {
    var self = this;
    self.amis[call.pbxid].action
    ({
        'action': 'setvar',
        'actionid': generate_key(),
        'channel': call.channel,
        'variable': 'TYPEMUSIC',
        'value': typemusic
    }, function (err, res) {


        self.amis[call.pbxid].action
        ({
            'action': 'setvar',
            'actionid': generate_key(),
            'channel': call.channel,
            'variable': 'CLASSMUSIC',
            'value': classmusic
        }, function (err, res) {


            self.amis[call.pbxid].action
            ({
                'action': 'setvar',
                'actionid': generate_key(),
                'channel': call.channel,
                'variable': 'PHLINE',
                'value': phline
            }, function (err, res) {

                self.amis[call.pbxid].action
                ({
                    'action': 'setvar',
                    'actionid': generate_key(),
                    'channel': call.channel,
                    'variable': 'DOMAIN',
                    'value': domain
                }, function (err, res) {
                    if (err) {
                        log.error(util.inspect(err));
                        log.error(call);
                        return cb_func("error")
                    }
                    self.amis[call.pbxid].action
                    ({
                        'action': 'setvar',
                        'actionid': generate_key(),
                        'channel': call.channel,
                        'variable': 'SAY',
                        'value': say
                    }, function (err, res) {
                        if (err) {
                            log.error(util.inspect(err));
                            log.error(call);
                            return cb_func("error")
                        }
                        self.amis[call.pbxid].action
                        ({
                            'action': 'redirect',
                            'actionid': generate_key(),
                            'channel': call.channel,
                            'context': 'CONNECTDST',
                            'exten': did,
                            'priority': 1
                        }, function (err, res) {
                            if (err) {
                                log.error(util.inspect(err));
                                log.error(call);
                                return cb_func("error")

                            }
                            else {
                                return cb_func(null, "ok")
                            }
                        });


                    });
                });
            });
        });
    });
};

//Проигрывает файл(ы) в канале call.channel
//параметр file может являтся массивом
//в этом случае событие 'PLAYBACKFINISHED' дойдет до observer.js
//только после проигрывания последнего файла
//evt - передается только при рекурсивном вызове для передачи евента в observer.js

Manager.prototype.doPlay = function (call, file, notify, timeout) {
    var self = this;
    var logger = self.logger;
    //notify - признак того что playback выполняется для нотификации
    //при true - в астериске будет выполнен в отдельном контексте с
    //генерированием специального набора событий
    if (!file) {
        logger.info('Nothing to play (not specified of file variable), skipping...');
        return true;
    }

    var readtimeout;
    if (!timeout) {
        readtimeout = 0;
    } else {
        readtimeout = timeout;
    }

    var nowplay = '';

    if (typeof file == 'string') {
        //Если в качестве параметра была передана строка
        nowplay = file;
    } else {
        //Если в качестве параметра был передан массив
        //Забираем первый элемент
        nowplay = file.join('&');
    }
    ;

    var playbackcontext;

    if (notify) {
        playbackcontext = 'PLAYBACKNOTIFY';
    } else {
        playbackcontext = 'PLAYBACK';
    }

    //Выставляем переменную канала 'NOWPLAY'
    self.amis[call.pbxid].action
    ({
        'action': 'setvar',
        'actionid': generate_key(),
        'channel': call.channel,
        'variable': 'NOWPLAY',
        'value': nowplay
    }, function (err, res) {
        if (err) {
            log.error('NOWPLAY ERROR');
            log.error(util.inspect(err));
            log.error(call);
            self.amis[call.pbxid].action
            ({
                'action': 'Status',
                'actionid': generate_key(),
                'channel': call.channel
            });


        } else {
            //Перенаправление вызова в контекст проигрывания файла
            self.amis[call.pbxid].action
            ({
                    'action': 'setvar',
                    'channel': call.channel,
                    'variable': 'READTIMEOUT',
                    'actionid': generate_key(),
                    'value': readtimeout
                }

                , function (err, res) {
                    if (err) {
                        log.error('READTIMEOUT ERROR');
                        self.amis[call.pbxid].action
                        ({
                            'action': 'Status',
                            'channel': call.channel
                        });
                        log.error(util.inspect(err));
                        log.error(call);
                    }
                    //Перенаправление вызова в контекст проигрывания файла
                    self.amis[call.pbxid].action
                    ({
                        'action': 'redirect',
                        'channel': call.channel,
                        'context': playbackcontext,
                        'exten': 's',
                        'priority': 1
                    });
                });
        }
    });
};

Manager.prototype.doNotify = function (id, domain, did, tries, timeout, actionid, phoneline, order_number, cb_func) {
    var self = this;
    var managers = self.getActiveManagersForTenant(domain);
    var pbxid = managers[Math.floor(Math.random() * managers.length)];
    log.child({widget_type: 'doNotify'});
    log.info({req_id: id}, 'originate в астериск', 'dst', did, 'phoneline:', phoneline);
    //Параметр не должен быть undefined, иначе возникнет ошибка приведения типов
    if (typeof order_number === 'undefined') {
        order_number = '';
    }
    //Выставляет переменную канала AGENT - формат - AOR оператора
    self.doOriginate(pbxid, did, 'notify', 'NOTIFY', 's',
        {
            'ORDERID': id,
            'DOMAIN3': config.domain,
            'DOMAIN4': domain,
            'PHLINE': phoneline,
            'ORDERNUMBER': order_number
        }
        , tries, timeout, actionid);

    return cb_func();

};

Manager.prototype.doSipShowRegistry = function (pbxid, cb_func) {
    var self = this;
//    var managers = self.getActiveManagersForTenant(domain);
//    var pbxid = managers[Math.floor(Math.random() * managers.length)];
    log.child({widget_type: 'doSipShowRegistry'});
    //log.info({req_id: id}, 'originate в астериск', 'dst', did, 'phoneline:', phoneline);
    if (self.amis[pbxid] == undefined) return cb_func({status:"error", message:"pbxid not found"});
    if (!self.amis[pbxid].isConnected()) return cb_func({status:"error", message:"pbx not connected"});
    self.amis[pbxid].action
    ({
            'action': 'SIPshowregistry'

        }
        , function (err, res) {
            if (err) {
                //log.error("Ошибка в ami(SIPshowregistry):", err);
                return cb_func(err);
            }
            if (res) {
              return cb_func(null, res);
            }

            if (res.response == 'Error') {

            }
        });

};

Manager.prototype.doOriginate = function (pbxid, did, clid, context, exten, setvar, tries, timeout, actionid) {
    var self = this;
    log.info("doOriginate:" + pbxid);
    self.amis[pbxid].action
    ({
            'action': 'originate',
            'actionid': actionid,
            'channel': 'Local/' + did + '@ORIGINATOR',
            'context': context,
            'callerid': '"' + clid + '@' + setvar.DOMAIN4 + '.' + setvar.DOMAIN3 + '" <' + clid + '>',
            'exten': exten,
            'priority': 1,
            'async': true,
            'variable': setvar

        }
        , function (err, res) {
            if (err) {
                log.error("Ошибка в ami(doOriginate):", err);
                //log.error(call);
            }
            if (res) {
                log.info(res);
            }

            if (res.response == 'Error') {
                if (tries - 1 > 0) {
                    setTimeout(self.doOriginate(pbxid,
                        did,
                        clid,
                        context,
                        exten,
                        setvar,
                        tries - 1,
                        timeout), 1000 * timeout);
                } else {
                    self.emit('daemon', pbxid, {'type': 'ORIGINATEFAILED', 'reason': res.reason, 'variables': setvar});
                }
            }
        });


};

Manager.prototype.moduleReload = function (pbx, module) {
    var self = this;

    self.amis[pbx].action
    ({
        'action': 'reload',
        'module': module
    });
};

Manager.prototype.execCli = function (pbx, command) {
    var self = this;
    var logger = self.logger;
    log.info('execCli asterisk ' + pbx + ' command: ' + command);
    self.amis[pbx].action
    ({
        'action': 'command',
        'command': command
    });
};

function isPositive(number) {
    return number > 0;
}

function spl(active) {
    log.info('active ', active);
    let asteriskStatus = [];
    let asteriskName = [];
    let promise;

    for (let i in active) {

        if (typeof (active[i]) !== "boolean") {
            asteriskName.push(active[i]);
        } else {
            asteriskStatus.push(active[i]);
        }
    }
    log.info("dd", asteriskStatus);
    promise = new Promise(function (resolve, reject) {
        resolve(asteriskName);
    });
    return promise


}

//Воспроизводит КПВ в канале call.channel
Manager.prototype.doRedirect = function (call, context, priority, exten) {
    var self = this;
    //Перенаправляется в контекст в котором запускается MOH и ренерируется событие
    self.amis[call.pbxid].action
    ({
            'action': 'redirect',
            'actionid': generate_key(),
            'channel': call.channel,
            'context': context,
            'exten': exten,
            'priority': priority
        }, function (err, res) {
            if (err) {
                //log.error(call.restid,util.inspect(err));
                log.warn(call.restid, 'Ami pbx for action doRedirect', {'response': err.response}, {'message': err.message});

            } else {
                log.info(call.restid, 'Ami pbx for action doRedirect', {'response': res.response}, {'message': res.message});
            }
        }
    );
};

Manager.prototype.doPlayNumber = function (call, number) {
    var self = this;
    //Выставляет переменную канала AGENT - формат - AOR оператора
    self.amis[call.pbxid].action
    ({
        'action': 'setvar',
        'actionid': generate_key(),
        'channel': call.channel,
        'variable': 'ORDERNUMBER',
        'value': number
    }, function (err, res) {
        if (err) {
            log.warn(call.restid, 'Ami pbx for action doPlayHangup', {'response': err.response}, {'message': err.message});
        } else {
            //Редирект в канал для выполнения Dial и перехвата исключений
            self.amis[call.pbxid].action
            ({
                'action': 'redirect',
                'actionid': generate_key(),
                'channel': call.channel,
                'context': 'SAYNUMBER',
                'exten': s,
                'priority': 1
            });
            log.info(call.restid, 'Ami pbx for action doPlayHangup', {'response': res.response}, {'message': res.message});
        }
    });
};

module.exports = Manager;
