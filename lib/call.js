//Объект телефонного звонка
//Используется для сохранения переменных и
//маппинга id рест с id канала в Asterisk
function call(pbxid, evt, parent) {
    var self = this;
    //проверка на массив, для совместимости с 13 версией астериска, ранее была строка
    var channel = Object.prototype.toString.call(evt.channel) === '[object Array]' && evt.channel.length > 0 ? evt.channel[0] : evt.channel;
    self.pbxid = pbxid;
    //Почему то в asterisk-manger uniqueid это массив
    self.callid = evt.uniqueid[0];
    //RESTID генерируется из уникального id звонка внутри Asterisk и имени самого Asterisk'а
    self.restid = self.pbxid + '_' + self.callid;
    self.channel = channel;
    self.did = evt.did;
    self.clidnum = evt.clidnum;
    self.clidname = evt.clidname;
    self.starttime = evt.timestamp;
    //Стейт вызова - по сути применятся для отладки в данный момент
    self.state = 'init';
    //Вызов - родитель
    //В момент соединения с оператором -
    //содается новый звонок с уникальными id
    //однако логически он выполняется в контексте
    //предыдущего вызова
    //Если parent == 'undefined' - значит этот звонок до соединения с оператором
    self.parent = parent;
    //Вызов после соединения с оператором
    self.child = undefined;
    self.notify = undefined;
    self.frominfo = evt.frominfo;
}

module.exports = call;
