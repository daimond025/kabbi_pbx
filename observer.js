'use strict';
const fs = require('fs');
//checkLogFile();
const url = require('url');
const util = require('util');
const nodemailer = require('nodemailer');
const lo = require('./lib/logFileChecker');
const myConfig = require('./config.js');
const mode = process.env.NODE_ENV;
const config = new myConfig(mode);
const d = require('domain').create();
const Restserver = require('./lib/restserver');
const Restclient = require('./lib/restclient');
const Manager = require('./lib/ami');
const Call = require('./lib/call.js');
const phoneLine = require('./lib/phoneLine.js');
const Rpc = require('./lib/rpc.js');
const Notify = require('./lib/notify.js');
const Notifybox = require('./lib/notifybox.js');
const Dbmanager = require('./lib/dbmanager');
const crypto = require('crypto');
const log = config.log.child({widget_type: 'observer.js'});
const serviceInfo = require('./lib/serviceInfo');


//уровень отладки меняется POSTом(http://m1.uatgootax.ru:8088/trace), level=DEBUG
//по умолчанию - INFO 
log.info("Текущий уровень отладки : " + config.log.level());

var generate_key = function () {
    const sha = crypto.createHash('sha1');
    sha.update(Math.random().toString());
    return sha.digest('hex');
};
d.on('error', function (er) {
        if (er) {
            if (typeof (er.message) !== 'undefined' && typeof (er.message) !== null) {
                log.error('Что-то сломалось, критическая ошибка: ', er.message);
            }
            log.error('Стэк ошибки: ', er.stack);
            log.error(config.mail_support_login);
            const transporter = nodemailer.createTransport({
                service: config.mail_support_service,
                auth: {
                    user: config.mail_support_login,
                    pass: config.mail_support_password
                }
            });
            const now_date_time = new Date();
            const mailOptions = {
                from: config.mail_support_login, // sender address
                to: config.mail_support_login, // list of receivers
                subject: 'Error in Voip Node worker ' + config.servertype, // Subject line
                text: er.message, // plaintext body
                html: er.message + "  |  " + er.stack // html body
            };
            // send mail with defined transport object
            transporter.sendMail(mailOptions, function (error, info) {
                if (error) {
                    log.error(error);
                } else {
                    log.error('Message sent: ' + info.response);
                }
            });
        }

    }
);
d.run(function () {
    var server = new Restserver(config.restserver);
    var client = new Restclient(config.restclient);
    var dbmanager = new Dbmanager(config.proxy, config.pbxes, config.proxy.concat(config.pbxes))
    var rpcClient = new Rpc(config.proxy);
    var box = new Notifybox();
    //Хэш вызовов
    var calls = {};
    var phoneLineStates = {};
//Поднимаем всех сконфирурированых в config.js манагеров
    var manager = new Manager(config.pbxes, log);
    log.info("Start new instance of voip-api");
//Слушаем события на Asterisk'ах
    manager.on('pbx', function (pbxid, evt) {
            var log = config.log.child({widget_type: 'manager.on observer.js'});
            //проверяем является поле uniqueid массивом или строкой, и берем либо 1 элемент массива, либо строку
            let uniqueid = Object.prototype.toString.call(evt.uniqueid) === '[object Array]' && evt.uniqueid.length > 0 ? evt.uniqueid[0] : evt.uniqueid;
            if (evt.event) {
                log.debug({req_id: evt.uniqueid}, 'received from aster', evt);
            }
            //
            if (evt.event == "RegistryEntry") {
                phoneLineStates[evt.username] = new phoneLine(pbxid, evt);
                   // {username:evt.username, domain:evt.domain, status:evt.status};
            }

            //Если вызов заврешен, отправляем на логирование в phone-api
            if (evt.event == "Cdr" && evt.lastapplication == "Dial") {
                var restid = pbxid + '_' + uniqueid;
                client.restsendNotify(evt, restid, log);
            }
            if (evt.event == "Reload") {
                log.debug("node recived event reload from asterisk." + evt.message + " try to reload db");
                setTimeout(startReload, 500);
            }
            if (evt.event == "ChannelReload") {
                log.debug("sip module recived event reload from asterisk. reason:" + evt.reloadreason + " try to reload db");
                setTimeout(startReload, 500);
            }


            //для канала иногда происходит маскарад, в этом случаи меняется идентификатор канала
            //отслеживаем Original ,Clone идентификаторы канала  и меняем
            if (evt.event == "Masquerade") {
                log.info("Masquerade", evt);
                ///log.info("orderid:" + evt.orderid + " Masquerade for channel original:" + evt.original + " Clone: " + evt.clone);
                log.info('calls for Masquerade = ', calls);
                //var orderId = box.getrestid(evt.original, calls);
                for (var i in calls) {
                    if (calls[i].channel === evt.original) {
                        calls[i].channel = evt.clone;
                        log.info('afer clone', calls[i]);
                    }
                }

                var orderId = box.getorderid(evt.original);
                log.info('ORDER for Masquerade = ' + orderId);
                log.info('data in calls for  Masquerade = ' + calls[orderId]);

                //calls[orderId].channel = evt.clone;
                log.debug('ORDER CHANNEL = ' + orderId);
                // log.debug(util.inspect(calls, {showHidden: true, depth: null}));
            }
            if (evt.event == "Dial") {
                var channel = Object.prototype.toString.call(evt.channel) === '[object Array]' && evt.channel.length > 0 ? evt.channel[0] : evt.channel;
                var order = box.getorderid(channel, calls)
                log.info(pbxid + '_' + evt.uniqueid, 'Receive ami event Dial,', 'subevent', evt.subevent, ',dialstatus:', evt.dialstatus, {req_id: order});
            }

            if (evt.event == "DialEnd") {
                if (typeof evt.calleridnum !== "undefined") {
                    let CallerIDNum = evt.calleridnum;
                    if (CallerIDNum.indexOf('op_') > -1) {
                        log.info("dispatcher", CallerIDNum, "DialEnd");
                        //restAction(calls[restid], 'agentBusy', evt, null);
                        client.restSendConnectAgentStatus(CallerIDNum, "free", "outcoming", function (err, data) {
                            if (err) {
                                log.error("restSendConnectAgentStatus", err);
                            }
                            log.info("Change agent status is success" +
                                "", data)
                        });
                    }

                }
            }
            if (evt.event == "OriginateResponse") {
                var order = box.getorderactid(evt.actionid);
                log.info("Received OriginateResponse: " + evt.response + " for order" + order);

                if (evt.response == "Failure") {
                    log.info({req_id: order}, 'OriginateResponse', evt.response, 'reason', evt.reason);
                    log.info("I'm delete order in box:" + order);
                    log.debug(box[order]);
                    //удалим из calls
                    finishCall(box.getrest(order));
                    //удалим из бокса
                    box.drop(order);

                } else {
                    log.info(evt.response + " - Originate Response" + uniqueid);
                }
            }

            if (evt.event == "UserEvent") {
                //Генериуется restid из имени уника вызова (состоит из ид вызова и домена астера)
                var restid = pbxid + '_' + uniqueid;
                //log.info(restid + " New notify for outgoing call for order_id=" + evt.orderid + " recive event: " + evt.userevent);
                //log.info(restid, {req_id: restid}, " New userevent recieved - " + evt.userevent);
                if (typeof evt.orderid !== "undefined") {
                    var orderDataInfo = "on order " + evt.orderid;
                } else {
                    var orderDataInfo = "";
                }
                log.debug(restid + ": NEW EVENT " + orderDataInfo + " " + util.inspect(evt));
                var channel = Object.prototype.toString.call(evt.channel) === '[object Array]' && evt.channel.length > 0 ? evt.channel[0] : evt.channel;
                //Инициирующие евенты
                switch (evt.userevent) {

                    case 'INCOMINGCALL':
                        //Новый входящий вызов
                        calls[restid] = new Call(pbxid, evt);
                        log.info(restid + " Ami recived event INCOMINGCALL.try to restAction");
                        restAction(calls[restid], 'incoming');
                        break;
                    case 'OUTGOINGCALL':
                        //Новый исходящий вызов
                        calls[restid] = new Call(pbxid, evt);
                        log.info("new Call create", restid)
                        break;
                    case 'NEWNOTIFY':
                        //Новый входящий вызов
                        calls[restid] = new Call(pbxid, evt);
                        log.info({req_id: evt.orderid}, 'event ', {req: evt.userevent}, " from aster ");
                        log.info(restid + " New notify " + evt.orderid + " start event try created new notify object, ch: " + channel);

                        box.setchan(evt.orderid, channel, pbxid, restid);
                        log.debug("CALLS from newnotify restid -" + restid + ":");
                        log.debug(util.inspect(calls, {showHidden: true, depth: null}));
                        break;
                    case 'NOTIFYCONNECT':
                        // calls[restid] = new Call(pbxid, evt);
                        tryrestid = box.getrest(evt.orderid);
                        calls[tryrestid].notify = box.get(evt.orderid);
                        calls[tryrestid].channel = channel;
                        function startN() {
                        processNotify(calls[tryrestid], 'start')
                        }
                        setTimeout(startN, 200);

                        break;
                    case 'CONNECTAGENTSUCCESS':
                        //Если поступил эвент от вызова которого нет в хэше
                        //и событие не 'INCOMINGCALL' (не новый вызов)
                        //Значит это вызов порожденный во время сединения с оператором
                        //и надо создать новый вызов и слинковать его с родителем
                        var parent = pbxid + '_' + evt.parentid;
                        //  log.info({req_id: parent}, {req: evt.userevent}, restid + " New userevent recieved");
                        calls[restid] = new Call(pbxid, evt, calls[parent]);
                        calls[parent].child = calls[restid];
                        restAction(calls[restid], 'connectAgentSuccess', evt, null);
                        log.info({req_id: restid}, 'Call connect to agent :', evt.agent);
                        log.info(restid + " Call connect agent success created child call object " + calls[restid]);
                        break;
                }


                //Обработка событий по типам
                switch (evt.userevent) {
                    case 'NOTIFYACTION':
                        log.debug("NOTIFYACTION(useraction in ivr) for orderid: " + evt.orderid);
                        log.info({req_id: evt.orderid}, evt.userevent, 'result: ', evt.result);
                        //Нажата клаваиша во время прослушивания информатора
                        var tryrestid = box.getrest(evt.orderid);
                        log.info("Restid: " + restid + " Tryrestid" + tryrestid);
                        log.debug(calls);
                        if (typeof tryrestid !== "undefined") {

                            log.info("1" + restid + " Notify action in state" + calls[tryrestid].notify.status_id + " result is [" + evt.result + "] ");
                            processNotifyKey(calls[tryrestid].notify.status_id, evt.result, tryrestid);
                        } else {
                            //видимо где-то потеряли orderid?
                            var tryrestid = restid;
                            log.info("2" + restid + " Notify action in state" + calls[restid].notify.status_id + " result is [" + evt.result + "] ");
                            processNotifyKey(calls[tryrestid].notify.status_id, evt.result, restid);
                        }
                        break;
                    case 'NOTIFYHANGUPCALL':
                        //Положена трубка во время работы автоинформатора
                        var tryrestid = box.getrest(evt.orderid);
                        log.info("Положена трубка во время работы автоинформатора",tryrestid);
                        if (typeof tryrestid !== "undefined") {


                            if (typeof calls[tryrestid].notify.status_id !== "undefined") {
                                log.info(tryrestid + " Notify hangup in status " + calls[tryrestid].notify.status_id);
                                log.info({req_id: evt.orderid}, evt.userevent, ' status order', calls[tryrestid].notify.status_id, 'Положена трубка во время работы автоинформатора');
                            } else {
                                log.debug(tryrestid + " Notify hangup in state " + calls[tryrestid]);
                                log.info({req_id: evt.orderid}, evt.userevent, 'Положена трубка во время работы автоинформатора');
                            }
                        } else {
                            //если tryrestid = undefined
                            var tryrestid = restid;
                            if (typeof tryrestid  === 'undefined')
                                break;
                            if (typeof calls[tryrestid] === 'undefined')
                                break;

                            log.info(tryrestid + " Notify hangup in status " + calls[tryrestid].notify.status_id);
                        }

                        
                        //обработка логиги прослушал ли уведомление
                        processHangupNotify(tryrestid);
                        //Чистим кучу
                        // от объектов звонка и нотификации
                        //чистим box
                        finishNotify(calls[tryrestid]);
                        ////чистим calls
                        finishCall(tryrestid);
                        break;
                    case 'CHANANSWERED':
                        //Поднята трубка на Asterisk
                        //Меняем стейт
                        calls[restid].state = 'answered';
                        //Сообщаем в va
                        restAction(calls[restid], 'actcomplete', 'ok');
                        break;
                    case 'CONNECTAGENTFAIL':
                        //Не удалось соедиенить с оператором
                        restAction(calls[restid], 'connectFail', evt);
                        break;
                    case 'CONNECTDSTFAIL':
                        //Не удалось соедиенить с удаленным номером
                        log.info(restid, " Ami recivied - CONNECTDSTFAIL", {req_id: evt.orderid});
                        restAction(calls[restid], 'connectDstFail', evt);
                        break;
                    case 'PLAYBACKSTARTED':
                        log.info(restid, " Ami recivied - PLAYBACKSTARTED", {req_id: evt.orderid}, 'event', evt.userevent, 'nowplay:', evt.nowplay);
                        break;
                    case 'PLAYBACKFINISHED':
                        //Проигрывание файла(ов) завершено
                        log.info(restid + " Ami recivied - PLAYBACKFINISHED");

                        if (calls[restid].notify) {
                            //Если это автоинформатор
                            switch (calls[restid].notify.state) {
                                case 'init':
                                    //Если текущее состояние init - отправляем в проговаривание моассива orderfiles
                                    //и высиавляем статус - прослушано
                                    calls[restid].notify.state = 'listened';
                                    processNotify(calls[restid], 'order');
                                    break;
                                case 'order':
                                    //Если - текущее состояние - order(абонент прослушал марку и номер авто)
                                    //Отправляем к массиву repeat - пояснения для клавиш DTMF
                                    calls[restid].notify.state = 'repeat';
                                    processNotify(calls[restid], 'repeat');
                                    break;
                            }
                        } else {
                            //Если это обычный вызов - меняем статус и отправляем в va
                            //факт того что действие выполнено
                            calls[restid].state = 'playfinished';
                            restAction(calls[restid], 'actcomplete', 'ok');
                        }
                        break;
                    case 'RINGINGSTARTED':
                        //Воспроизведение КПВ началось
                        if (calls[restid].state == 'inqueue') {
                            //Кпв в очереди
                            log.info(restid + " Ami recived - RINGINGSTARTED(Ringing started on channel)");
                        } else {
                            //Кпв в сценарии вызова
                            calls[restid].state = 'ringing';
                            log.info(restid + " Call start channel ringing");
                            //Сообщаем в va о завершении действия
                            restAction(calls[restid], 'actcomplete', 'ok');
                        }
                        break;
                    case 'MOHSTARTED':
                        //Воспроизведение Музыки на ожидании началось
                        if (calls[restid].state == 'inqueue') {
                            //В очереди
                            log.info(restid + " Call start inqueue moh");
                        } else {
                            //В сценарии
                            calls[restid].state = 'moh';
                            log.info(restid + " Call start channel ringing");
                            //Сообщаем в VA о завершении действия
                            restAction(calls[restid], 'actcomplete', 'ok');
                        }
                        break;
                    case 'QUEUEHANGUPCALL':
                        //Вызов разован в очереди
                        log.info(restid + " Ami recived QUEUEHANGUPCALL.(Hangup call in queue)");
                        calls[restid].state = 'hangup';
                        //Сообщаем в VA о завершении вызова во время нахождения вызова в очереди
                        restAction(calls[restid], 'hangup', 'init', evt.orderid);
                        log.info(restid, {req_id: evt.orderid}, 'положена трубка в очереди ожидания');
                        //finishCall(restid, evt.orderid);
                        break;

                    case 'QUEUETIMEOUT':
                        //Кончилось время ожидания в очереди
                        log.info(restid + " timeout in queue call");
                        //Сообщаем в VA о завершении вызова во время нахождения вызова в очереди
                        restAction(calls[restid], 'actcomplete', 'ok');
                        log.info({req_id: evt.orderid}, 'положена трубка в очереди ожидания');
                        //finishCall(restid, evt.orderid);
                        break;


                    case 'HANGUPORIGINATOR':
                        log.info({req_id: evt.orderid}, 'Call hangup call in context ORIGINATOR, hangupcause:', evt.hangupcause);
                        break;

                    case 'HANGUPCALLCONNECTEDTOOPERATOR':
                        restAction(calls[restid], 'agentFree', evt, null);
                        break;
                    case 'SETSTATUS':
                        log.info("status agent", evt.statusagent);
                        switch (evt.statusagent) {
                            case 'BUSY':
                                client.restSendConnectAgentStatus(evt.calleridnum, "busy", "outcoming", function (err, data) {
                                    let response = JSON.parse((JSON.stringify(data)));
                                    if (response.result == '1') {

                                        manager.doRedirect(calls[restid], evt.context[0], "outallowed", evt.exten);
                                    }
                                    else if (response.result == '0') {
                                        manager.doRedirect(calls[restid], evt.context[0], "outnotallowed", evt.exten);
                                    }
                                    else {
                                        log.error("unknown answer for set status, dump:", response)
                                    }
                                });
                                break;
                            case 'FREE':
                                client.restSendConnectAgentStatus(evt.calleridnum, "free", "outcoming", function (err, data) {
                                    if (err) {
                                        log.error(err);
                                        return;
                                    }
                                    let response = JSON.parse((JSON.stringify(data)));
                                    if (response.result == '1') {
                                        log.info("Normal status change for ", uniqueid);
                                    }
                                    else if (response.result == '0') {
                                        log.warn("status niot changed for agnet", evt.agent, uniqueid);
                                    }
                                })
                                break;
                        }
                        break;
                    case 'HANGUPCALL':
                        //Вызов разован в одном из пунктов сценария или автоинформатора
                        //или при разговоре с оператором
                        var tryrestid = box.getrest(evt.orderid);
                        log.info(restid, ' Recived ami event - HANGUPCALL', 'tryrestid: ', tryrestid, {req_id: evt.orderid});
                        log.debug({req_id: evt.orderid}, 'HANGUPCALL', calls, 'restid', restid);
                        if (calls[restid]) {

                            restAction(calls[restid], 'hangup', 'complete', function () {
                                //Подчищаем массивы вызовов и нотификаций
                                finishCall(restid);
                                delete calls[restid];

                            });
                        } else {
                            log.warn(restid + " HANGUPCALL call  not found, call not delete.  tryrestid:", tryrestid);
                            //log.debug(calls);
                            //finishCall(tryrestid);
                            box.drop(evt.orderid);
                            if (tryrestid === undefined) {
                                log.warn(restid, " HANGUPCALL tryrestid=undefined nothing for finishCall");
                            }
                            else {
                                finishCall(tryrestid);
                            }
                            delete calls[tryrestid];
                            //log.error("HANGUPCALL - calls obj calls after delete:", calls);
                        }
                        break;
                    default:
                        break;
                }
            }
        }
    );
    manager.on('daemon', function (pbxid, evt) {
        //Это события генерирыемые непосредствено этим скриптом(не появившиеся в ASTERISK или REST SERVER)
        switch (evt.type) {
            case 'ORIGINATEFAILED':
                //По какойто причине не удалось инициировать автооповещение
                //Например,отсутвуют каналы или все заняты или у клиента отключен телефон
                log.warn('Failed originate evt on pbx ' + pbxid + util.inspect(evt));
                var notify = box.get(evt.variables.ORDERID);
                client.restNotifyfinished(notify.order_id,
                    notify.status_id,
                    notify.domain,
                    'noanwser', restid, log);
                box.drop(evt.variables.ORDERID);
                break;
        }
    });
//Вызовы REST от app сервера
    server.on('connectagent', function (req, res) {
        log.info({req_id: req.url.callid}, {req: req.url}, " rest from app ");
        log.debug(util.inspect(req.url, {showHidden: true, depth: null}));
        //App сервер требует отправить вызов на  оператора
        log.info(url.parse(req.url, true).query.callid + ' App server call to operator connect ' + url.parse(req.url, true).query.agent);
        if (calls[url.parse(req.url, true).query.callid]) {
            manager.connectAgent(calls[url.parse(req.url, true).query.callid], url.parse(req.url, true).query.agent, log,
                function (err, results) {
                    if (err) {
                        res.send({state: 'restidnotfound', message: err.message,});
                    } else {
                        res.send({state: 'ok'});
                    }
                }
            );

        } else {
            log.info("call not found, we send state: restidnotfound");
            res.send({state: 'restidnotfound'});
        }
    }).on('notify', function (req, res) {
        var actionid = generate_key();
        var phoneLine = 0;
        var did = 0;
        if (typeof req.body.company_phone === 'undefined' && typeof req.body.phone_line === 'undefined') {
            res.send({state: 'error', message: 'company_phone and phone_line is undefined'});
            return false;
            phoneLine = 0;
        }
        else if (typeof req.body.company_phone === 'string') {
            if (req.body.company_phone != 'null')
                phoneLine = req.body.company_phone;
            else {
                phoneLine = req.body.phone_line;
            }

        }
        else if (typeof req.body.company_phone === 'undefined')
        {
            if (typeof req.body.phone_line === 'string') {
                if (req.body.phone_line === '') {
                    res.send({state: 'error', message: 'param phone_line is empty'});
                    return false;
                }
                phoneLine = req.body.phone_line;
            } else {
                phoneLine = 0;
            }
        }

        if (!phoneLine) {
            //пишем в лог ошибку
            log.error({req_id: req.body.order_id}, 'UNDEFINE phone_line in request:', {
                    req: req.method +
                    JSON.stringify(req.headers.host) + JSON.stringify(req.baseUrl) + JSON.stringify(req.body)
                },
                " from app make notify for order  ", req.body.order_id, " in status ", req.body.status_id,
                " call in domain ", req.body.domain, " actionid: ", actionid, " by phone line: ", req.body.phone_line);
            res.send({state: 'error', message: 'error in parametrs  '});
            return
        } else {
            log.info({req_id: req.body.order_id}, {
                    req: req.method + JSON.stringify(req.headers.host)
                    + JSON.stringify(req.baseUrl) + JSON.stringify(req.body)
                }, " from app make notify for order  ",
                req.body.order_id, " in status ", req.body.status_id, " call in domain ", req.body.domain, " actionid: ",
                actionid, " by phone line: ", req.body.phone_line);

        }
        //Тут инициируется новый вызов
        if (config.flagAutoCallsToDriver) {
            did = req.body.driver_phone
        } else {
            did = req.body.did
        }
        if (box.push(req.body, actionid)) {
            manager.doNotify(req.body.order_id, req.body.domain,did, req.body.call_count_try, req.body.call_wait_timeout, actionid, phoneLine, req.body.order_number, function () {
                res.send('[{"state": "ok"}]');
                log.info({req_id: req.body.order_id}, {res: res.statusCode + ' ' + res.statusMessage});
            });
        } else {
            log.error({req_id: req.body.order_id}, "уведомление для заказа" + req.body.order_id + "уже есть. звонить повторно не будем!");
            res.send({state: 'error', message: 'notifycation for this order avaible'});
        }
    }).on('updatesipdomains', function (req, res) {
        //Метод перезагружает sip домены из базы Kamailio в Asterisk
        loadSipDomains();
        res.send('OK');
    }).on('version', function (req, res) {
        serviceInfo.version(res);
    }).on('list', function (req, res) {
        //Дебагер - выводит хэш звонков через api (используется для отладки)
        res.send(util.inspect(calls));
        log.info("req:", req.baseUrl, "res:", util.inspect(calls));
    }).on('status', function (req, res) {
        serviceInfo.status(manager, dbmanager, res)

    }).on('trace', function (req, res) {
        var level = JSON.parse((JSON.stringify(req.body)));
        switch (level.level) {
            case 'DEBUG':
                res.send('OK');
                log.level("DEBUG");
                log.info("сменили уровень отладки на DEBUG");
                config.log.level('DEBUG');
                //log.info('debug level',config.log.level());
                break;
            case 'INFO':
                res.send('OK');
                log.level("INFO");
                log.info("сменили уровень отладки на INFO");
                //log.info('debug level',config.log.level());//
                break;
            default:
                res.send('FAIL');
                log.level("INFO");
                log.info("не удалось сменить уровень отладки. установим уровень INFO");
        }

    }).on('updatesipuser', function (req, res) //этот метод дергает phone-
    {
        log.debug("app server req update sip user:" + req.query.username);
        //если не передали имя значит добавили нового юзера

        if (typeof req.query.username === "undefined") {
            log.info("loadSipUsers = undefined(app server create new user), whe reload all user from db");
            loadSipUsers();
            res.send('OK');
        }

        //если имя есть, значит обновили пароль
        else {
            loadSipUser(req.query.username, function (err, results) {
                    if (err) {
                        res.send({state: 'error', message: err});
                    }
                    if (results) {
                        res.send({state: 'ok'});
                    }
                }
            );
            res.send({state: 'ok'});
        }
    }).on('reloaduacreg', function (req, res) //этот метод дергает phone-
        {
            var param = JSON.parse((JSON.stringify(req.body)));
            var host = param.host;

            if (typeof param.host === "undefined") {
                log.error("reloaduacreg = undefined, pidor request ?");
                res.send({state: 'error', message: 'EMPTY_PARAM_HOST'});
            }

            //если имя есть, значит обновили пароль
            else {
                rpcClient.sendCommand(host, 'uac.reg_reload',
                    function (err, results) {
                        if (err) {
                            log.error("err:", err.message);
                            res.send({state: 'error', message: err.code});
                        }
                        if (results) {
                            log.info("results for send rpc command:", results);
                            res.send({state: 'ok', message: results});
                        }
                    }
                )
            }
        }
    ).on('phoneLine', function (req, res) //добавляет uac регистрации на сервера
        {
            log.info("body:", req.body);
            var param = JSON.parse((JSON.stringify(req.body)));
            log.info ("param: ", param);
            var username = param.r_username;
            var domain = param.r_domain;
            var realm = param.realm;
            var auth_username = param.auth_username;
            var password = param.auth_password;
            var sipServerLocal = param.sipServerLocal;
            var sipServerRemote = param.auth_proxy;
            var phoneLine = param.phoneLine;
            var typeServer = param.typeServer;
            var typeQuery = param.typeQuery;
            var phoneLineId = param.phoneLineId;
            var tenantId = param.tenantId;
            var port = param.port;

            //res.send({state: 'ok', message: 'ok'});
            //return;

            if (typeQuery == 'LIST'){
                phoneLineStates = {};
                 manager.doSipShowRegistry(sipServerLocal,function (err,results) {
                     if (err){
                         log.error(err);
                         res.send(err);
                         return;

                     }
                     log.info(results);
                         setTimeout(function() {res.send(phoneLineStates)}, 500);
                 }

                 )




                }

            if (username === undefined) {
                //res.send({state: 'error', message: 'EMPTY_PARAM_username'});
                log.info("EMPTY_PARAM_username");
            }
            else if (password === undefined) {
                //res.send({state: 'error', message: 'EMPTY_PARAM_password'})
                log.info('EMPTY_PARAM_password');
            }
            else if (domain === undefined) {
                //res.send({state: 'error', message: 'EMPTY_PARAM_domain'});
                log.info('EMPTY_PARAM_domain');
            }

            else if (sipServerLocal === undefined) {
                //res.send({state: 'error', message: 'EMPTY_PARAM_sipServerLocal'});
                log.info('EMPTY_PARAM_sipServerLocal');
            }
            else if (sipServerRemote === undefined) {
                log.info('EMPTY_PARAM_sipServerLocal');
                // res.send({state: 'error', message: 'EMPTY_PARAM_sipServerRemote'})
            }
            else if (realm === undefined) {
                //res.send({state: 'error', message: 'EMPTY_PARAM_realm'})
                log.info('EMPTY_PARAM_REALM');
            }
            else if (phoneLine === undefined) {
                res.send({state: 'error', message: 'EMPTY_PARAM_phoneLine'})
                log.info('EMPTY_PARAM_PHONELINE');
            }
            else if (typeServer === undefined) {
                res.send({state: 'error', message: 'EMPTY_PARAM_typeServer'})
            }
            else if (tenantId === undefined) {
                //  res.send({state: 'error', message: 'EMPTY_PARAM_tenantId'})
                log.info('req phoneLine : EMPTY_PARAM_tenantId');

            }
            else if (phoneLineId === undefined) {
                res.send({state: 'error', message: 'EMPTY_PARAM_phoneLineId'})
            }
            else if (port === undefined) {
                //res.send({state: 'error', message: 'EMPTY_PARAM_PORT'})
                log.info('EMPTY_PARAM_PORT');
            }
            switch (typeQuery) {
                case 'CREATE':
                    log.info("PhoneLine - CREATE");
                    if (typeServer == 'kamailio') {

                        dbmanager.createPhoneLineInKamailio(username, password, domain, sipServerLocal, sipServerRemote, realm, phoneLine, phoneLineId, tenantId, port,
                            function (err, results) {
                                log.info("results:", results);
                                if (err) {
                                    log.error("err:", err.message);
                                    res.send({state: 'error', message: err.code});
                                }
                                if (results) {
                                    rpcClient.sendCommand(sipServerLocal, 'uac.reg_reload',
                                        function (err, results) {
                                            if (err) {
                                                log.error("err:", err.message);
                                                res.send({state: 'error', message: err.code});
                                            }
                                            if (results) {
                                                log.info("results for send rpc command:", results);
                                                res.send({state: 'ok', message: results});
                                            }
                                        }
                                    )

                                }
                                else if (results == '0') {
                                    res.send({state: 'error', message: 'The record already exists'});
                                }
                            }
                        )
                    }
                    if (typeServer == 'asterisk') {
                        dbmanager.createPhoneLineInAsterisk(username, password, domain, sipServerLocal, sipServerRemote, realm, phoneLine, phoneLineId, tenantId, port,
                            function (err, results) {
                                if (err) {
                                    res.send({state: 'error', message: err.message, code: err.code});
                                    return;
                                }
                                if (results) {
                                    manager.execCli(sipServerLocal, 'sip reload');
                                    //manager.execCli(sipServerLocal, 'sip show peer ' + username + ' load');
                                    res.send({state: 'ok', message: results});
                                }
                                else if (results == '0') {
                                    res.send({state: 'error', message: 'The record already exists?'});
                                }
                            }
                        )
                    }
                    break;
                case 'UPDATE':
                    log.info("addPhoneLine - UPDATE");
                    if (typeServer == 'kamailio') {
                        // //массив со значениями для вставки в БД
                        // var value = Object.keys(param).map(function (key) { return param[key]; });
                        // //массив с ключами ля вставки в БД
                        // var keys = Object.keys(param);
                        dbmanager.updatePhoneLineInKamailio(param, sipServerLocal,
                            function (err, results) {
                                log.info("results:", results);
                                if (err) {
                                    log.error("err:", err.message);
                                    res.send({state: 'error', message: err.code});
                                }
                                if (results) {
                                    rpcClient.sendCommand(sipServerLocal, 'uac.reg_reload',
                                        function (err, results) {
                                            if (err) {
                                                log.error("err:", err.message);
                                                res.send({state: 'error', message: err.code});
                                            }
                                            if (results) {
                                                log.info("results for send rpc command:", results);
                                                res.send({state: 'ok', message: results});
                                            }
                                        }
                                    )
                                    // res.send(results);
                                }
                                else if (results == '0') {
                                    res.send({state: 'error', message: 'The record not found'});
                                }
                            }
                        )
                    }
                    else if (typeServer == 'asterisk') {
                       // res.send({state: 'error', message: 'method not implement'});
                       // return;
                        dbmanager.updatePhoneLineInAsterisk(param, sipServerLocal,
                            function (err, results) {
                                log.info("results:", results);
                                if (err) {
                                    log.error("err:", err.message);
                                    res.send({state: 'error', message: err.code});
                                }
                                if (results) {
                                    // тут должен быть sip-релоад
                                    manager.execCli(sipServerLocal, 'sip reload');
                                    res.send({state: 'ok', message: results});
                                    // res.send(results);
                                }
                                else if (results == '0') {
                                    res.send({state: 'error', message: 'The record not found'});
                                }
                            }
                        )
                    }
                    else {
                        res.send({state: 'error', message: 'method not implement'});
                    }
                    break;
                case 'DELETE':
                    if (typeServer == 'kamailio') {
                        dbmanager.deletePhoneLineInKamailio(phoneLineId, sipServerLocal,
                            function (err, results) {
                                log.info("results:", results);
                                if (err) {
                                    log.error("err:", err.message);
                                    res.send({state: 'error', message: err.code});
                                }
                                if (results) {
                                    rpcClient.sendCommand(sipServerLocal, 'uac.reg_reload',
                                        function (err, results) {
                                            if (err) {
                                                log.error("err:", err.message);
                                                res.send({state: 'error', message: err.code});
                                            }
                                            if (results) {
                                                log.info("results for send rpc command:", results);
                                                res.send({state: 'ok', message: results});
                                            }
                                        }
                                    )
                                    // res.sen  d(results);
                                }
                                else if (results == '0') {
                                    res.send({state: 'error', message: 'The record not found'});
                                }
                            }
                        )
                    }
                    else if (typeServer == 'asterisk') {
                        dbmanager.deletePhoneLineInAsterisk(phoneLineId, sipServerLocal,
                            function (err, results) {

                                if (err) {
                                    log.error("err:", err.message);
                                    res.send({state: 'error', message: err.code});
                                }
                                if (results) {
                                    log.info("results:", results);
                                    // перезагружаем модуль sip-релоад
                                    manager.execCli(sipServerLocal, 'sip reload');
                                    res.send({state: 'ok', message: results});
                                }
                                else if (results == '0') {
                                    res.send({state: 'error', message: 'The record not found'});
                                }
                            }
                        )
                    }
                    else  {
                         res.send({state: 'error', message: 'type server unknown'});
                         return;

                    }
                    log.info("addPhoneLine - DELETE");
                default:
                    break;
            }
        }
    ).on('connectdst', function (req, res) {
        log.info({req_id: req.url.restid}, {req: req.url}, " rest from app ");
        log.debug(util.inspect(req.url, {showHidden: true, depth: null}));
        //App сервер требует соеденить вызов с удаленным номером
        //TODO обработчит как у get запроса ?
        var restid = url.parse(req.url, true).query.restid
        var did = url.parse(req.url, true).query.phone
        var phline = url.parse(req.url, true).query.phline;
        var domain = url.parse(req.url, true).query.domain;
        var typemusic = url.parse(req.url, true).query.typemusic;
        var classmusic = url.parse(req.url, true).query.classmusic;
        var timeout = url.parse(req.url, true).query.timeout;
        if (restid === undefined) {
            res.send({state: 'error', message: 'restid = undefined'});
            return;
        }
        if (did === undefined) {
            res.send({state: 'error', message: 'phone = undefined'});
            return;
        }
        if (phline === undefined) {
            res.send({state: 'error', message: 'phline = undefined'});
            return;
        }
        if (domain === undefined) {
            res.send({state: 'error', message: 'domain = undefined'});
            return;
        }
        if (timeout !== undefined) {
            if (parseInt(timeout.trim()) < '10') {
                res.send({state: 'error', message: 'timeout must be > 10s'});
                return;
            }
        }
        else {
            res.send({state: 'error', message: 'timeout must be defined'});
        }
        if (typemusic !== undefined) {
            if (typemusic.trim() !== 'r' && typemusic.trim() !== 'm') {
                res.send({state: 'error', message: 'typemusic = wrong'});
                return;
            }
            if ((typemusic.trim() === 'm') && (classmusic.trim() === '')) {
                res.send({state: 'error', message: 'classmusic = undefined'});
                return;
            }
        }
        else {
            res.send({state: 'error', message: 'typemusic = undefined'});
        }
        if (calls[restid] === undefined) {
            res.send({state: 'restidnotfound', message: 'restid not found'});
            return;
        }
        else {
            log.info(restid + ' App server requescall to dst ' + did);
            manager.doDial(calls[restid], did, domain, null, phline, typemusic, classmusic, timeout,
                function (err, results) {
                    if (err) {
                        res.send({state: 'restidnotfound', message: err.message});
                    } else {
                        res.send({state: 'ok'});
                    }
                }
            )
        }

    })
    ;
//Функция отправляющая события в app сервер
    var restAction = function (call, method, param, order) {
        var log = config.log.child({widget_type: 'restAction observer.js'});
        //log.info("temp debug", call);
        //Обработка по типам
        switch (method) {
            case 'incoming':
                //Входящий вызов
                client.restIncoming(call.restid, call.did, call.clidnum, call.frominfo, log,
                    function (action) {
                        //В коллбэке выполняет следующее задание от app
                        log.debug(call.restid + " Rest server response " + action);
                        callAction(call, action);
                    });
                break;
            case 'connectFail':
                //Соединение с оператором не удалось
                if (param.reason == 'CANCEL') {
                    //Если пользвоатель сам бросил трубку
                    log.info(call.restid + " Connect agent cancel proccess " + param);
                    client.restConnectAgentFail(call.restid, param.agent, 'cancel', log,
                        function () {
                            finishCall(call.restid);
                        });
                } else {
                    //Если не удалось по другим причинам(оператор отбил сам, не взял по таймауту, неполдаки на РМ оператора)
                    client.restConnectAgentFail(call.restid, param.agent, 'congestion', log,
                        function (action) {
                            //В коллбэке выполняет следующее задание от app
                            log.info(call.restid + " Rest server response " + action);
                            callAction(call, action);
                        });
                }
                break;
            case 'connectDstFail':
                //Соединение с удаленным номером не удалось
                if (param.reason == 'CANCEL') {
                    //Если пользвоатель сам бросил трубку
                    log.info(call.restid + " Connect agent cancel proccess " + param);
                    client.restComplete(call.restid, 'cancel', log,
                        function () {
                            finishCall(call.restid);
                        });
                } else {
                    //Если не удалось по другим причинам(оператор отбил сам, не взял по таймауту, неполдаки на РМ оператора)
                    client.restComplete(call.restid, param.reason, log,
                        function (action) {
                            //В коллбэке выполняет следующее задание от app

                            if (action === '') {
                                log.warn(call.restid + " No action found");
                            } else {
                                log.info(call.restid + " Rest server response " + action);
                                callAction(call, action);
                            }
                        });
                }
                break;
            case 'actcomplete':
                //Действие выполнено
                client.restComplete(call.restid, param, log,
                    function (action) {
                        //В коллбэке выполняет следующее задание от app сервер
                        log.info(call.restid + " Rest server response " + action);
                        if (action === '') {
                            log.warn(call.restid + " No action found");
                        } else {
                            callAction(call, action);
                        }
                    });
                break;
            case 'hangup':
                //Соеинение разорвано
                log.info(call.restid, "Hangup call started:");
                client.restHangup(call.restid, 'pbx', param, log,
                    function () {
                        finishCall(call.restid, order);
                        log.info(call.restid, "Hangup call end.");
                    });
                break;

            case 'agentFree':
                client.restSendConnectAgentStatus(param.agent, "free", "incoming", function (err, data) {
                    if (err) {
                        log.error(err)
                    }
                    log.info("dd3", data);
                });
                break;
            case 'connectAgentSuccess':
                //Соеинение разорвано
                //log.info(call.restid,"Hangup call started:" );
                client.restSendConnectAgentStatus(param.agent, "busy", "incoming", function (err, data) {
                    if (err) {
                        log.error(err)
                    }
                    log.info("dd4", data);
                });
                break;
            default:
                //Остальные методы
                log.warn(call.restid + " UNKNOWN METHOD [" + method + "]");
                new Error('undefined method');
        }
    };
    let processNotify = function (call, method) {
        switch (method) {
            case 'start':
                //Обработка обязательной фазы оповещения
                log.info(call.notify + " Notify started playing welcome part");
                if (config.flagAutoCallsToDriver) {
                    manager.doPlay(call, call.notify.welcome, false, 0.1);
                } else {
                    manager.doPlay(call, call.notify.welcome, true, 0.1);
                }
                break;
            case 'order':
                //Обработка фазы сообщающей данные автомобиля
                log.info(call.notify + " Notify continue playing order part");
                if (call.notify.orderfiles != null) {
                    manager.doPlay(call, call.notify.orderfiles, true, 0.1);
                } else {
                    processNotify(call, 'repeat');
                }
                break;
            case 'repeat':
                //Обработка фазы когда предлагаются возможные варианты действия пользователя
                //Соединение с водителем, оператором, отказ от заказа, продление заказа
                log.info(call.notify + " Notify continue playing repeat part");
                manager.doPlay(call, call.notify.repeatfiles, true, 5);
                break;
            case 'saynumber':
                //проиграть номер в канал
                manager.doPlayNumber(call, call.notify.order_number);
                break;

        }
    };
//Функция отправляет задичи в Asterisk
    let callAction = function (call, action) {
        //Парсим параметры
        var log = config.log.child({widget_type: 'callAction observer.js'});
        var json = JSON.parse(action);
        log.info(call.restid + " New action for pbx " + JSON.stringify(json));
        //Обработка по типам
        switch (json.action) {
            case 'doAnswer':
                //Ответить на вызов
                manager.doAnswer(call.did, call, 'answer', json.domain);
                break;
            case 'doProgress':
                //Ответить на вызов
                manager.doProgress(call.did, call, 'answer', json.domain);
                break;

            case 'doPlay':
                //Проиграть файл(файлы)
                call.state = 'playback';
                manager.doPlay(call, json.file, false);
                break;
            case 'doRinging':
                //Начать генерацию КПВ
                manager.doRinging(call, 150);
                break;
            case 'doMoh':
                //Запустить музыку на ожидании
                manager.doMoh(call, json.mclass, 150);
                break;
            case 'doQueue':
                //Постановка в очередь

                call.state = 'inqueue';
                if (json.type == 'r') {
                    manager.doRinging(call, config.qtimeout);
                } else {
                    manager.doMoh(call, json.mclass, config.qtimeout);
                }
                break;
            case 'connectAgent':
                //Соединение с оператором
                log.debug("connectAgent" + json.agent);
                manager.connectAgent(call, json.agent, log, function (err, result) {
                    if (err) {
                        log.error("connect agent", err)
                    }
                });
                break;
            case 'doHangup':
                manager.doHangupChannel(call.channel, call.pbxid, '2');
                //TODO fix leg to  /call/hangup?callid=media1_media1.lcl-1484828807.450&leg=pbx&state=api
                //restAction(call.restid, 'hangup', call.state, null);
                break
            case 'doInfo':
                //Входящий автоинформатор
                //log.debug("");
                log.debug('doInfo!!!, JSON:', json, call.clidname);
                box.push(json, call.clidname);
                calls[call.restid].notify = box.get(json.order_id);
                var active = box.check(json.order_id);
                if (active) {
                    manager.doHangupChannel(active.channel, active.pbxid, '2');
                }
                //Запускаем автоинформатор
                processNotify(calls[call.restid], 'start');
                break;
            case 'Err':
                log.error('Error ' + call + ' ' + action);
                break;
            case 'doRedirect':
                if (json.classmusic === undefined) {
                    json.classmusic = null;
                }
                manager.doDial(call, json.phone, json.domain, null, json.phline, json.typemusic, json.classmusic, json.timeout,
                    function (err, res) {
                        if (err) {
                            log.error({state: 'restidnotfound', message: err.message});
                        } else {
                            log.info({doRedirect: 'ok'}, json.phone, json.domain, json.phline);
                        }
                    });
                break;
            default:
                //Остальеые типы запросов
                log.warn(call.restid + "UNHANDLED METHOD" + JSON.stringify(json));
        }
    };
//Обработка действий клиета в автоинформаторе
    let processNotifyKey = function (status, key, id) {
        log.info('processNotifyKey', key, id);
        ////Реакция на клавиши - зависит от статуса
        //функция checkRepeat() возвращает true - если клиент прослушал repeat files
        //Вызывается чтобы проигнорировать пустой ответ автоинформатора после прослушивания массива orderfiles
        switch (status) {
            //Новый заказ
            case '1':
            case '2':
            case '3':
                switch (key) {
                    case '':
                        //Не нажал ничего
                        if (!checkRepeat(id)) {
                            actDialService(id);
                        }
                        break;
                    case '0':
                        actCancel(id);
                        break
                    default :
                        //Вызывается если пользователь нажал неверную клавишу
                        processNotify(calls[id], 'repeat');
                        break;

                }
                break;
            //Свободный заказ
            case '4':
                switch (key) {
                    case '':
                        //Не нажал ничего
                        if (!checkRepeat(id)) {
                            actDialService(id);
                        }
                        break;
                    case '0':
                        actCancel(id);
                        break;
                    default :
                        //Вызывается если пользователь нажал неверную клавишу
                        processNotify(calls[id], 'repeat');
                        break;
                }
                break;
            //Водитель принял заказ
            case '17':
                switch (key) {
                    case '':
                        if (!checkRepeat(id)) {
                            actDialService(id);
                        }
                        break;
                    case '1':
                        actDialDriver(id);
                        break;
                    case '0':
                        actCancel(id);
                        break;
                    default :
                        processNotify(calls[id], 'repeat');
                        break;
                }
                break;
            //Машина прибыла
            case '26':
                switch (key) {
                    case '':
                        if (!checkRepeat(id)) {
                            actDialDriver(id);
                        }
                        break;
                    case '1':
                        actDialService(id);
                        break;
                    case '0':
                        actCancel(id);
                        break;
                    default :
                        processNotify(calls[id], 'repeat');
                        break;
                }
                break;
            //Клиент не выходит
            case '27':
                switch (key) {
                    case '':
                        if (!checkRepeat(id)) {
                            actDialDriver(id);
                        }
                        break;
                    case '1':
                        actDialService(id);
                        break;
                    case '0':
                        actCancel(id);
                        break;
                    default :
                        processNotify(calls[id], 'repeat');
                        break;
                }
                break;
            //Заказ отменен
            case '39':
                actCancel(id);
                break;
            //Заказ просрочен
            case '52':
                switch (key) {
                    case '':
                        if (!checkRepeat(id)) {
                            actExtend(id);
                        }
                        break;
                    default :
                        processNotify(calls[id], 'repeat');
                        break;
                }
                break;
            //Машина опазыдывает
            case '54':
                switch (key) {
                    case '':
                        if (!checkRepeat(id)) {
                            actDialService(id);
                        }
                        break;
                    case '1':
                        var dialres = actDialDriver(id);
                        if (!dialres) {
                            log.warn(id + ' Не найден ' + status + ' key ' + key, ' Звонок будет отправлен оператору ');
                            actDialService(id);
                        }
                        break;
                    case '0':
                        actCancel(id);
                        break;
                    default :
                        processNotify(calls[id], 'repeat');
                        break;
                }
                break;
            default:
                log.warn(id + ' UNHANDLED STATUS ' + status + ' key ' + key, ' we call send to operator ');
                actDialService(id);
                break;
        }

    };
    let actCancel = function (id) {
        //Отмена заказа
        client.restNotifyfinished(calls[id].notify.order_id,
            calls[id].notify.status_id,
            calls[id].notify.domain,
            'cancel', calls[id].restid, log);
        manager.doPlayHangup(calls[id], calls[id].notify.cancel);
    };
    let actExtend = function (id) {
        //Продление заказа
        log.info({req_id: calls[id].notify.order_id}, " Try to prolongate order from notify ", id);
        client.restNotifyfinished(calls[id].notify.order_id,
            calls[id].notify.status_id,
            calls[id].notify.domain,
            'extend', calls[id].restid, log);
        manager.doPlayHangup(calls[id], calls[id].notify.success);
    };
    let actGetNext = function (id) {
        //Взять следующее действие(когда входящий автоинформатор и нужно соеденить с оператором)
        log.info(id + " Finished incoming notify try to get next job from call scenario");
        restAction(calls[id], 'actcomplete', 'ok');
    };
    let actDialService = function (id) {
        //Соединение со службой такси из автоинформатора
        log.info({req_id: calls[id]}, 'Try to dial on dispetcher from notify');
        if (calls[id].notify.isInfo) {
            //Если это входящий звонок из информатора (по логике 
            //заданной в астериске, это входящий звонок с признаком frominfo = 1
            //
            //
            //Забираем следующее действие из сценария
            restAction(calls[id], 'actcomplete', 'ok');
            //delete calls[id].notify; //TODO
        } else {
            //Если это автодозвон - отправляем в коллцентр
            log.info(id + " actDialService ");
            manager.dialService(calls[id], calls[id].notify.phone_line, calls[id].notify.callerid, calls[id].notify.agentfile);
            //Отмечаемся в апи об успешном соединении
            client.restNotifyfinished(calls[id].notify.order_id,
                calls[id].notify.status_id,
                calls[id].notify.domain,
                'connectagent', calls[id].restid, log);
            log.info(id + " Delete call and notify objects");
            finishCall(id);
        }

    };
    let checkRepeat = function (id) {
        //Если это исходящий информатор
        //Если в данный момент проговорили только данные по заказу
        if (calls[id].notify.state == 'listened') {
            //Меняем стейт на следующий и отправляем договаривать
            //набор repeatfiles
            calls[id].notify.state = 'repeat';
            processNotify(calls[id], 'repeat');
            log.info({req_id: calls[id].notify.order_id}, 'checkRepeat');
            return true;
        } else if (calls[id].notify.state == 'init') {
            //Меняем стейт на следующий и отправляем договаривать
            //набор repeatfiles
            calls[id].notify.state = 'listened';
            processNotify(calls[id], 'order');
            return true;
        } else {
            return false;
        }
    };
    let actDialDriver = function (id) {
        log.child({widget_type: 'actDialDriver'});
        //Вызов водителя из автоинформатора
        let phone = calls[id].notify.driver_phone;
        log.info({req_id: calls[id].notify.order_id}, 'Try to dial driver from notify', 'driver_phone: ', phone, ' domain ', calls[id].notify.domain);
        if (phone === null || phone === undefined) {
            log.warn("dial to driver is " + calls[id].notify.driver_phone + " try to dial dispetcher");
            //actDialService(id);
            //return 0
        } else {
            //return 1
            log.info("dial to driver is " + calls[id].notify.driver_phone + " try to dial dispetcher");
            //(call,                  did,                domain,                  say,                       phline,typemusic,classmusic,timeout,cb_func)
            manager.doDial(calls[id], calls[id].notify.driver_phone, calls[id].notify.domain, calls[id].notify.driverfile, null, null, null, 60, function (err, results) {
                if (err) {
                    log.error({state: 'restidnotfound', message: err.message});
                } else {
                    log.info({state: 'ok'});
                }
            });
        }

    };
    let processHangupNotify = function (restid) {
        if (calls[restid].notify.state == 'listened' || calls[restid].notify.state == 'repeat') {
            //Стейт listened или repeat означает что пользователь успел прослушать обязательную часть
            if (calls[restid].notify.status_id == '52') {
                //52 статус - хэнгап означает отмену заказа
                actCancel(restid, true);
            } else {
                //для остальных стейтов это просто нормальное завершение информатора
                client.restNotifyfinished(calls[restid].notify.order_id,
                    calls[restid].notify.status_id,
                    calls[restid].notify.domain,
                    'ok', restid, log);
                sendHangupIfInfo(calls[restid].notify);

            }
        } else {
            //Пользователь не прослушал обязательную часть
            //Сообщаем в gootax об этом
            client.restNotifyfinished(calls[restid].notify.order_id,
                calls[restid].notify.status_id,
                calls[restid].notify.domain,
                'nolisten', restid, log);
            sendHangupIfInfo(calls[restid].notify)
        }
    };
    let finishCall = function (restid, order) {
        log.child({widget_type: 'finishCall  observer.js'});

        if (calls[restid]) {
            log.info(restid, "FinishCall. calls[restid] - found. all calls:");

            if (calls[restid].notify) {
                log.info(restid, "FinishCall() calls[restid].notify => 1");
                finishNotify(calls[restid]);
            }
            if (calls[restid].child) {
                log.info(restid, "FinishCall [calls[restid].child - true(у вызова есть потомок удалим его:)" + calls[restid].child.restid);
                delete calls[calls[restid].child.restid];
            }
            if (calls[restid].parent) {
                log.info(restid, "FinishCall calls[calls[restid].parent - true (у вызова есть родитель, удалим его:)" + calls[restid].child.restid);
                delete calls[calls[restid].parent.restid];
            }
            log.info(restid, "FinishCall - try to delete calls" + calls[restid]);
            delete calls[restid];

        } else {
            //эта фигня для отладки, когда не удаляется вызов
            var tryrestid = box.getrest(order);
            log.warn(restid, "FinishCall -  CALL not found. not finishCall", "orderid:" + order, " tryrest id:" + tryrestid);
        }
    };
    let finishNotify = function (call) {
        log.child({widget_type: 'finishNotify'});
        var actionid = generate_key();
        if (box.shift(call.notify.order_id)) {
            //что делает эта хрень ? 
            box.setchan(call.notify.order_id, undefined);
            var notify = box.get(call.notify.order_id);
            // id, domain, did, tries, timeout, actionid, phoneline, childLogger
            //зачем 2 раз оповещать клиента ? 
            manager.doNotify(notify.order_id, notify.domain, notify.callerid, notify.call_count_try, notify.call_wait_timeout, actionid, notify.phone_line, log, function () {
                log.info({req_id: call.notify.order_id}, 'Started buffered notify');
            });
        } else {
            //удаляем заказ из box'а
            log.info({req_id: call.notify.order_id}, 'box drop(delete) this call ');
            box.drop(call.notify.order_id);
        }
    };
    let sendHangupIfInfo = function (notify, restid) {
        //Завершает вызов если это автоинформатор
        if (notify.isInfo) {
            restAction(calls[restid], 'hangup', 'init');
        }
    };
    //TODO Это плохо работает, так как не все домены загрузил в базу  ,
    //TODO а уже релоадит  в астере
    let loadSipDomains = function () {
        //Загрузка доменов из Kamailio
        dbmanager.getDomains

        (
            function (domains) {
                dbmanager.updateNodes
                (
                    domains, function (affected) {
                        domains.forEach
                        (
                            function (domain) {
                                config.pbxes.forEach(function (item, i, arr) {
                                    var pbxLabel = item.id;
                                    manager.execCli(pbxLabel, 'sip show peer ' + domain + ' load');
                                });
                            }
                        );
                    }
                );
            }
        );
    };
    const loadSipUsers = function () {
        log.child({widget_type: 'loadSipUsers'});
        //Загрузка юзеров из Kamailio
        dbmanager.getUsers
        (
            function (userList) {

                for (var i in userList) {
                    log.info(" -LoadSipUsers. Load sip user from kamailio. username:" + userList[i].name);
                    dbmanager.updateAster(userList[i].name, userList[i].password, userList[i].domain, function () {
                    });
                }
                ;

            }
        );
    };

    let loadSipUser = function (username, cb_func) {
        //Загрузка юзеров из Kamailio
        dbmanager.getUser(username, function (err, results) {
            if (err) {
                log.error(" loadSipUser fo user " + username + " result: " + err);
                return cb_func("ERROR");
            } else {
                log.info("started loadSipUser fo user " + username);
                for (var i in results) {
                    dbmanager.pwdupdateinAster(results[i].username, results[i].md5_password, results[i].domain, function (err, results) {
                        if (err) {
                            log.error("pwdupdateinAster:", err.message)
                            return cb_func("ERROR");
                        }
                        for (var key in results) {
                            manager.execCli(key, 'sip prune realtime ' + username);
                            manager.execCli(key, 'sip show peer ' + username + ' load');
                        }
                    });
                }

            }
        });

    };



    function startReload() {
        phoneLineStates = {};
        loadSipDomains();   //Выгрузка доменов из kamailio и загрузка в astdb на ноду.
        loadSipUsers();  //Выгрузка пользователей из kamailio и загрузка в astdb
    }

    function isDef(v) {
        return v !== undefined && v !== null;
    }

    //Выгрузка доменов из kamailio и загрузка в astdb на ноду.
    //loadSipDomains();
    //Выгрузка пользователей из kamailio и загрузка в astdb 
    loadSipUsers();
    log.info("active manager: ", manager.getActiveManagersForTenant());
});