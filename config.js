'use strict';
var bunyan = require('bunyan');
var bsyslog = require('bunyan-syslog');
var ver = 'v1.0.11';

function myConfig(typeConfig) {
  return new ConfigProd();
}

function ConfigProd() {
    this.ver = ver;
    this.domain = 'v1.kabbi.local';
    this.servertype = "prod";
    this.pbxes =
            this.restserver = {port: 8088};
    this.restclient = {host: "va2.kabbi.local", port: 8086, protocol: "https://"};
    this.debug = true;
    this.proxy =
        [
            {
                id: "sp2",
                host: "sp2.kabbi.local",
                port: 5060,
                protocol: "http://",
                dbhost: "192.168.81.15",
                dbuser: "daimond",
                dbpassword: "123456",
                database: "k_kamailio"
            }
        ];
    //из этой БД берутся юзеры и загружаются в астериск
    this.defaultDB = 'sp2.kabbi.local';
    this.qtimeout = 150;
    this.ctimeout = 8;
    this.servertype = "prod";
    this.mail_support_login = "api_pbx@gootax.pro";
    this.mail_support_service = "Yandex";
    this.mail_support_password = "";
    this.log = bunyan.createLogger({
        name: 'observer.js',
        streams: [
            {
                level: 'info',
                type: 'raw',
                stream: bsyslog.createBunyanStream({
                    type: 'sys',
                    facility: bsyslog.local0,
                    name: 'api_pbx-1.0.11'
                })
            }
        ]
    });
    this.log.level("INFO");
}

module.exports = myConfig;
